#!/bin/bash
if [ -f /var/log/nginx/access.log.2.gz ]; then
	/bin/zcat /var/log/nginx/access.log.*.gz | /usr/bin/goaccess /var/log/nginx/access.log /var/log/nginx/access.log.1 - --log-format=COMBINED --ignore-crawlers -o /var/www/logserver/main/index.html
else 
	/usr/bin/goaccess /var/log/nginx/access.log /var/log/nginx/access.log.1 --log-format=COMBINED --ignore-crawlers -o /var/www/logserver/main/index.html
fi
#if [ -f /var/log/nginx/1.example.com.access.log.2.gz ]; then
#	/bin/zcat /var/log/nginx/1.example.com.access.log.*.gz | /usr/bin/goaccess /var/log/nginx/1.example.com.access.log /var/log/nginx/1.example.com.access.log.1 - --log-format=COMBINED --ignore-crawlers -o /var/www/logserver/tube/index.html
#else
#	/usr/bin/goaccess /var/log/nginx/1.example.com.access.log /var/log/nginx/1.example.com.access.log.1 --log-format=COMBINED --ignore-crawlers -o /var/www/logserver/tube/index.html
#fi
#if [ -f /var/log/nginx/2.example.com.access.log.2.gz ]; then
#	/bin/zcat /var/log/nginx/2.example.com.access.log.*.gz | /usr/bin/goaccess /var/log/nginx/2.example.com.access.log /var/log/nginx/2.example.com.access.log.1 - --log-format=COMBINED --browsers-file=/etc/goaccess/browsers_feed-reader.list --ignore-crawlers -o /var/www/logserver/rss-bridge/index.html
#else
#	/usr/bin/goaccess /var/log/nginx/2.example.com.access.log /var/log/nginx/2.example.com.access.log.1 --log-format=COMBINED --browsers-file=/etc/goaccess/browsers_feed-reader.list --ignore-crawlers -o /var/www/logserver/rss-bridge/index.html
#fi

exit 0
