#!/bin/sh
rclone mount \                    
    --read-only \
    --allow-non-empty \
    --allow-other \
    --stats 0 \
    --quiet \
    --acd-templink-threshold 0 \
    --buffer-size 1G \
    --checkers 16 \
    --timeout 5s \
    --max-read-ahead 1G \
    --contimeout 5s \
    --log-file=/var/log/rclone.log \
    3ncrypt:/ /mnt/ACD/3ncrypt/  &
