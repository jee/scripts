#!/bin/sh

#file="$1"
#watch_dir="/home/hexo/tmp"
#json_file="/home/hexo/tmp/_data/output_nanogallery.json"
watch_dir="/home/hexo/blog/source/static/photos"
json_file="/home/hexo/blog/source/_data/gallery.json"
hexo_dir="/home/hexo/blog"

func_watch () {
  inotifywait -m \
  -e create,moved_to \
  --format '%w%f' \
  "$watch_dir"
}

func_thumb () {
    convert -thumbnail 300 "$file" "$thumb_path"
}

exifmodel () {
  identify -verbose "$file" | grep "exif:Model:" | awk '{print $2" "$3}'
}

exifflash () {
  identify -verbose "$file" | grep "exif:Flash:" | awk '{print $2}'
}

exiffocallength () {
  identify -verbose "$file" | grep "exif:FocalLength:" | awk '{print $2}'
}
exiffstop () {
  identify -verbose "$file" | grep "exif:Fstop:" | awk '{print $2" "$3}'
}

exifexposure () {
  identify -verbose "$file" | grep "exif:ExposureTime:" | awk '{print $2}'
}

exifiso () {
  identify -verbose "$file" | grep "exif:ISOSpeedRatings:" | awk '{print $2}'
}

exiftime () {
  identify -verbose "$file" | grep "exif:DateTimeOriginal:" | awk '{print $2" "$3}'
}

exiflocation () {
  identify -verbose "$file" | grep "exif:GPSLocation:" | awk '{print $2" "$3}'
}

remove_2_last_lines () {
  echo "$(cat $json_file | head -n -2 $json_file)" > "$json_file"
}

insert_new_object () {
  echo '  },' >> "$json_file"
  echo '  {' >> "$json_file"
  echo '      "title":"'"${filename%.*}"'",'  >> "$json_file"
  echo '      "ngdesc":"'""'",'  >> "$json_file"
  echo '      "ngtags":"'"notag"'",'  >> "$json_file"
  echo '      "path":"'"$image_path"'",'  >> "$json_file"
  echo '      "ngthumb":"'"$thumb_path_relative"'",'  >> "$json_file"
  echo '      "ngexifmodel":"'"$(exifmodel)"'",'  >> "$json_file"
  echo '      "ngexifflash":"'"$(exifflash)"'",'  >> "$json_file"
  echo '      "ngexiffocallength":"'"$(exiffocallength)"'",'  >> "$json_file"
  echo '      "ngexiffstop":"'"$(exiffstop)"'",'  >> "$json_file"
  echo '      "ngexifexposure":"'"$(exifexposure)"'",'  >> "$json_file"
  echo '      "ngexifiso":"'"$(exifiso)"'",'  >> "$json_file"
  echo '      "ngexiftime":"'"$(exiftime)"'",'  >> "$json_file"
  echo '      "ngexiflocation":"'"$(exiflocation)"'"'  >> "$json_file"
  echo '  }'  >> "$json_file"
  echo ']'  >> "$json_file"
}
func_exiftojson () {
  remove_2_last_lines
  insert_new_object
}
func_hexo_generate () {
  cd "$hexo_dir"
  hexo generate
}

func_watch | \
while IFS= read -r file; do
  image_path="/static/photos/$(basename "$file")"
  filename=$(basename "$file")
  thumb_path="/home/hexo/blog/source/static/photos/thumbs/$filename"
  thumb_path_relative="/static/photos/thumbs/$filename"
  func_thumb
  func_exiftojson
  func_hexo_generate
done
