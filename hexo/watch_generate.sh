#!/bin/sh

#file="$1"
watch_dir="/home/hexo/blog/source"
hexo_dir="/home/hexo/blog"

func_watch () {
  inotifywait -m -r \
  -e create,moved_to,delete,close_write \
  --format '%w%f' \
  --excludei '(static/photos|_data/gallery.json)' \
  "$watch_dir"
}

func_hexo_generate () {
  cd "$hexo_dir"
  hexo generate
}

func_watch | \
while IFS= read -r file; do
  func_hexo_generate
done
