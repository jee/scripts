#!/bin/sh

#watch_dir="~/blog/source/static/photos"
watch_dir="/home/hexo/tmp"

func_watch () {
  inotifywait -m \
  -e create,moved_to \
  --format '%w%f' \
  "$watch_dir"
}

func_thumb () {
  convert -thumbnail 300 "$file" "$thumb_path" 
}

func_watch | \
while IFS= read -r file; do
  filename=$(basename "$file")
  filepath=$(dirname "$file")
  thumb_path="$filepath/thumbs/$filename"
  func_thumb
  echo "$file"
  echo "$thumb_path"
done
