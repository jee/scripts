#!/bin/bash

watch_dir="/mnt/data/Media/Music"

inotifywait -m -e create,moved_to,delete --format '%w%f' "$watch_dir" | while IFS= read -r dir_path; do
    /usr/bin/php7.0 /var/www/ampache/bin/catalog_update.inc -a Music
done
