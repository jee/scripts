#!/usr/bin/env python3

import json
import subprocess
import os
import sys

myEnv = os.environ.copy()
myEnv["BORG_PASSPHRASE"] = "<YOUR_PASSPHRASE>"
myEnv["BORG_REPO"] = "<YOUR_REPO_PATH>"


sourceRepo = "<YOUR_SOURCE_REPO>"
targetRepo = "<YOUR_TARGE_REPO>"

excludeFile = "YOUR_EXCLUDE_FILE_PATH"
excludeCmd = " --exclude-from " + excludeFile
myComment = '"migrate from' + sourceRepo + '"'


def runcommand(cmd):
    proc = subprocess.Popen(cmd,
         env=myEnv,
         stdout=subprocess.PIPE,
         stderr=subprocess.PIPE,
         shell=True,
         universal_newlines=True)
    std_out, std_err = proc.communicate()
    print(std_out)
    print(std_err)
    return proc.returncode, std_out, std_err


def getjson():
    cmd = "borg list " + sourceRepo + " --json"
    code, out, err = runcommand(cmd)

    return out


repoListJson = json.loads(getjson())
repoPath = repoListJson['repository']['location']

print(repoPath)



for repo in repoListJson['archives']:
    archiveName = repo['archive']
    archiveTime = repo['time']
    archiveTime = archiveTime[0:19]
    myWorkdir = "/volume1/Borg/extracted/" + archiveName

    print(archiveName + ' ' + archiveTime)
    mkdircmd = "mkdir " + myWorkdir
    infocmd = "borg info " + repoPath + '::' + archiveName + ' --json'
    extractcmd = "borg extract " + repoPath + '::' + archiveName + excludeCmd + " --progress "
    createcmd =  "borg create " + targetRepo + '::' + archiveName + ' ./ ' + excludeCmd + " --exclude-caches " + " --timestamp " + archiveTime + " --comment " + myComment + " --progress "
    removecmd = "rm -rf " + myWorkdir

    #print(runcommand(infocmd))

    print("---------------------------------------------------------------------------------------------------------------------------")
    print("Create " + myWorkdir + " directory")
    runcommand(mkdircmd)
    os.chdir(myWorkdir)
    print("Workdir Changed : " + os.getcwd())
    print("---------------------------------------------------------------------------------------------------------------------------")
    print("---------------------------------------------------------------------------------------------------------------------------")
    print("extract archive:" + archiveName +" in " + os.getcwd())
    runcommand(extractcmd)
    print("---------------------------------------------------------------------------------------------------------------------------")
    print("---------------------------------------------------------------------------------------------------------------------------")
    print("create new archive:" + archiveName +" in " + targetRepo)
    runcommand(createcmd)
    print("---------------------------------------------------------------------------------------------------------------------------")
    print("Remove " + archiveName + " directory")
    runcommand(removecmd)
    print("---------------------------------------------------------------------------------------------------------------------------")



