#!/bin/bash


function checkifisroot () {
  if [ "$EUID" -ne 0 ]; then
    echo "Sorry, you need to run this as root"
    exit 1
  fi
}

function install_deps() {
  apt update
  apt install -y \
    bc \
    openssl
}

function get_params () {
  RANDOM_SSH_PORT=$(echo $RANDOM % 1025 +  65535 | bc)
  read -e -i "$RANDOM_SSH_PORT" -p $'wich ssh port ?\n' SSH_PORT
  read -e -i "ssh-user" -p $'wich ssh group should be allowed to connect on ssh server ?\n' SSH_GROUP
  read -e -p $'should we create a new user to connect on ssh server type no or enter the new user name ?\n' NEW_USER
  if getent passwd $NEW_USER > /dev/null 2>&1; then
    echo "The user $NEW_USER already  exists, we will add $NEW_USER to $SSH_GROUP"
    EXISTING_USER="$NEW_USER"
  else
    echo "the user $NEW_USER will be created in next step"
  fi
  NETWORK_INTERFACE=$(ip -o link show | awk '{print $2,$9}' | grep 'UP' | awk '{print $1}' | sed 's/.$//')
  GET_IP=$(ifconfig $NETWORK_INTERFACE | awk '$1 == "inet" {print $2}')
}

function create_ssh_group () {
  groupadd -f --gid 2222 "$SSH_GROUP"
}

function create_ssh_user () {
  if [ ! -z $EXISTING_USER ];then
    usermod -a -G "$SSH_GROUP" "$EXISTING_USER"
  elif [ ! -z $NEW_USER ];then
    useradd --groups "$SSH_GROUP" --user-group --create-home "$NEW_USER"
    while
      passwd "$NEW_USER"
      retval=$?
    do
      if [ $retval -ne 0 ]; then
	echo "Hmmm there is something wrong do it again!"
      else
	break
      fi
    done
  fi
}

function copy_id () {
  echo "we need a ssh key"
  echo "please type \"ssh-copy-id $NEW_USER@$GET_IP\" on a new terminal"
  while
    read -p $"and type ENTER when it's done" ENTER
  do
    if [ -f /home/"$NEW_USER"/.ssh/authorized_keys ] && [ -s /home/"$NEW_USER"/.ssh/authorized_keys ]; then 
      echo "Ok content of /home/$NEW_USER/.ssh/authorized_keys is :"
      cat /home/"$NEW_USER"/.ssh/authorized_keys
      break
    elif [ ! -f /home/"$NEW_USER"/.ssh/authorized_keys ]; then
      echo "/home/$NEW_USER/.ssh/authorized_keys don't exist"
      echo "please type \"ssh-copy-id $NEW_USER@$GET_IP\" on a new terminal"
    elif [ ! -s /home/"$NEW_USER"/.ssh/authorized_keys ]; then
      echo "/home/$NEW_USER/.ssh/authorized_keys is empty"
      echo "please type \"ssh-copy-id $NEW_USER@$GET_IP\" on a new terminal"
    fi
  done
}

function moduli () {
  awk '$5 > 2000' /etc/ssh/moduli > "/tmp/moduli"
  wc -l "/tmp/moduli" >/dev/null 2>&1
  mv "/tmp/moduli" /etc/ssh/moduli
  echo "New moduli file generated"
}

function re-gen_sshhost_keys () {
  cd /etc/ssh
  rm ssh_host_*key*
  ssh-keygen -t ed25519 -f ssh_host_ed25519_key -N "" < /dev/null >/dev/null 2>&1
  ssh-keygen -t rsa -b 4096 -f ssh_host_rsa_key -N "" < /dev/null >/dev/null 2>&1
  echo "New Ssh Host Keys generated"
}

function write_sshd_config () {
  cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bak
  echo "Port $SSH_PORT" > /etc/ssh/sshd_config
  echo "#AddressFamily any" >> /etc/ssh/sshd_config
  echo "#ListenAddress 0.0.0.0" >> /etc/ssh/sshd_config
  echo "#ListenAddress ::" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "HostKey /etc/ssh/ssh_host_rsa_key" >> /etc/ssh/sshd_config
  echo "#HostKey /etc/ssh/ssh_host_ecdsa_key" >> /etc/ssh/sshd_config
  echo "HostKey /etc/ssh/ssh_host_ed25519_key" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# Ciphers and keying" >> /etc/ssh/sshd_config
  echo "#RekeyLimit default none" >> /etc/ssh/sshd_config
  echo "Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr" >> /etc/ssh/sshd_config 
  echo "" >> /etc/ssh/sshd_config
  echo "# Message authentication codes" >> /etc/ssh/sshd_config 
  echo "MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com" >> /etc/ssh/sshd_config 
  echo "" >> /etc/ssh/sshd_config
  echo "# Logging" >> /etc/ssh/sshd_config
  echo "#SyslogFacility AUTH" >> /etc/ssh/sshd_config
  echo "#LogLevel INFO" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# Authentication:" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "#LoginGraceTime 2m" >> /etc/ssh/sshd_config
  echo "PermitRootLogin no" >> /etc/ssh/sshd_config
  echo "#StrictModes yes" >> /etc/ssh/sshd_config
  echo "#MaxAuthTries 6" >> /etc/ssh/sshd_config
  echo "#MaxSessions 10" >> /etc/ssh/sshd_config
  echo "AllowGroups $SSH_GROUP" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "PubkeyAuthentication yes" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# Expect .ssh/authorized_keys2 to be disregarded by default in future." >> /etc/ssh/sshd_config
  echo "AuthorizedKeysFile	.ssh/authorized_keys .ssh/authorized_keys2" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "#AuthorizedPrincipalsFile none" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "#AuthorizedKeysCommand none" >> /etc/ssh/sshd_config
  echo "#AuthorizedKeysCommandUser nobody" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts" >> /etc/ssh/sshd_config
  echo "#HostbasedAuthentication no" >> /etc/ssh/sshd_config
  echo "# Change to yes if you don't trust ~/.ssh/known_hosts for" >> /etc/ssh/sshd_config
  echo "# HostbasedAuthentication" >> /etc/ssh/sshd_config
  echo "#IgnoreUserKnownHosts no" >> /etc/ssh/sshd_config
  echo "# Don't read the user's ~/.rhosts and ~/.shosts files" >> /etc/ssh/sshd_config
  echo "#IgnoreRhosts yes" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# To disable tunneled clear text passwords, change to no here!" >> /etc/ssh/sshd_config
  echo "PasswordAuthentication no" >> /etc/ssh/sshd_config
  echo "PermitEmptyPasswords no" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# Change to yes to enable challenge-response passwords (beware issues with" >> /etc/ssh/sshd_config
  echo "# some PAM modules and threads)" >> /etc/ssh/sshd_config
  echo "ChallengeResponseAuthentication no" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# Kerberos options" >> /etc/ssh/sshd_config
  echo "#KerberosAuthentication no" >> /etc/ssh/sshd_config
  echo "#KerberosOrLocalPasswd yes" >> /etc/ssh/sshd_config
  echo "#KerberosTicketCleanup yes" >> /etc/ssh/sshd_config
  echo "#KerberosGetAFSToken no" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# GSSAPI options" >> /etc/ssh/sshd_config
  echo "#GSSAPIAuthentication no" >> /etc/ssh/sshd_config
  echo "#GSSAPICleanupCredentials yes" >> /etc/ssh/sshd_config
  echo "#GSSAPIStrictAcceptorCheck yes" >> /etc/ssh/sshd_config
  echo "#GSSAPIKeyExchange no" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# Set this to 'yes' to enable PAM authentication, account processing," >> /etc/ssh/sshd_config
  echo "# and session processing. If this is enabled, PAM authentication will" >> /etc/ssh/sshd_config
  echo "# be allowed through the ChallengeResponseAuthentication and" >> /etc/ssh/sshd_config
  echo "# PasswordAuthentication.  Depending on your PAM configuration," >> /etc/ssh/sshd_config
  echo "# PAM authentication via ChallengeResponseAuthentication may bypass" >> /etc/ssh/sshd_config
  echo "# the setting of "PermitRootLogin without-password"." >> /etc/ssh/sshd_config
  echo "# If you just want the PAM account and session checks to run without" >> /etc/ssh/sshd_config
  echo "# PAM authentication, then enable this but set PasswordAuthentication" >> /etc/ssh/sshd_config
  echo "# and ChallengeResponseAuthentication to 'no'." >> /etc/ssh/sshd_config
  echo "UsePAM yes" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "#AllowAgentForwarding yes" >> /etc/ssh/sshd_config
  echo "#AllowTcpForwarding yes" >> /etc/ssh/sshd_config
  echo "#GatewayPorts no" >> /etc/ssh/sshd_config
  echo "X11Forwarding no" >> /etc/ssh/sshd_config
  echo "#X11DisplayOffset 10" >> /etc/ssh/sshd_config
  echo "#X11UseLocalhost yes" >> /etc/ssh/sshd_config
  echo "#PermitTTY yes" >> /etc/ssh/sshd_config
  echo "PrintMotd no" >> /etc/ssh/sshd_config
  echo "#PrintLastLog yes" >> /etc/ssh/sshd_config
  echo "#TCPKeepAlive yes" >> /etc/ssh/sshd_config
  echo "#PermitUserEnvironment no" >> /etc/ssh/sshd_config
  echo "#Compression delayed" >> /etc/ssh/sshd_config
  echo "#ClientAliveInterval 0" >> /etc/ssh/sshd_config
  echo "#ClientAliveCountMax 3" >> /etc/ssh/sshd_config
  echo "#UseDNS no" >> /etc/ssh/sshd_config
  echo "#PidFile /var/run/sshd.pid" >> /etc/ssh/sshd_config
  echo "#MaxStartups 10:30:100" >> /etc/ssh/sshd_config
  echo "#PermitTunnel no" >> /etc/ssh/sshd_config
  echo "#ChrootDirectory none" >> /etc/ssh/sshd_config
  echo "#VersionAddendum none" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# no default banner path" >> /etc/ssh/sshd_config
  echo "#Banner none" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# Allow client to pass locale environment variables" >> /etc/ssh/sshd_config
  echo "AcceptEnv LANG LC_*" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# override default of no subsystems" >> /etc/ssh/sshd_config
  echo "Subsystem	sftp	/usr/lib/openssh/sftp-server" >> /etc/ssh/sshd_config
  echo "" >> /etc/ssh/sshd_config
  echo "# Example of overriding settings on a per-user basis" >> /etc/ssh/sshd_config
  echo "#Match User anoncvs" >> /etc/ssh/sshd_config
  echo "#	X11Forwarding no" >> /etc/ssh/sshd_config
  echo "#	AllowTcpForwarding no" >> /etc/ssh/sshd_config
  echo "#	PermitTTY no" >> /etc/ssh/sshd_config
  echo "#	ForceCommand cvs server" >> /etc/ssh/sshd_config
}

function restart_sshd () {
  systemctl restart sshd.service
  echo "We will restart the ssh server but before close this terminal"
  echo "please check if you can connect to the server by typing in a new terminal:"
  echo "ssh -p $SSH_PORT -i \"Path of your private ssh key\" $NEW_USER@$GET_IP"
  echo "If you can't connect to the server you can go back to the old config by typing"
  echo "rm /etc/ssh/sshd_config && mv /etc/ssh/sshd_config.bak /etc/ssh/sshd_config && systemctl restart sshd.service"
}

# Check
checkifisroot

# Install Deps
install_deps

# Get Params
get_params

# Manage user
create_ssh_group
create_ssh_user

#Ssh Key
copy_id

# Moduli and Ssh host_keys
moduli
re-gen_sshhost_keys

# Write sshd_config
write_sshd_config

# Restart sshd
restart_sshd

exit 0
