#!/bin/sh

func_watch () {
	inotifywait -m \
	-e create,moved_to \
	--format '%w%f' \
        /mnt/data/torrents/seed/TvShows
}

func_filebot () {
    /opt/filebot/filebot.sh \
        -script dev:amc \
        --output "/" \
        --action hardlink \
        --conflict skip \
        -non-strict \
        --log-file "/home/torrent/.config/filebot/filebot.log" \
        --def excludeList="/home/torrent/.config/filebot/filebot.excludes" \
        --def unsorted=n artwork=y extras=n music=n \
        --def "seriesFormat=/mnt/data/Media/TvShows/{n}/Season {s.pad(2)}/{fn}" \
        --def "animeFormat=/mnt/data/Media/Animes/{n}/Season {s.pad(2)}/{fn}" \
        --def "movieFormat=/mnt/data/Media/Movies/{n} ({y})/{fn}" \
        "$file"
}

func_watch | \
while IFS= read -r file; do
    func_filebot
    if [ "$retval_filebot" != 0 ]; then
        sleep 300
        func_filebot
    fi
done
