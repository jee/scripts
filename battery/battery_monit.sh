#!/bin/bash

ac_state=$(cat /sys/class/power_supply/AC/online)

if [[ ${ac_state} == 0 ]]; then
	
	percentageLow="7"
	percentageCritical="5"
	percentageAction="2"
	
	bat_state=$(cat /sys/class/power_supply/BAT0/capacity)
	
	lowMessage="Your battery power is low ! less than ${bat_state} %"
	lowIcon="/usr/share/icons/Adwaita/scalable/status/battery-level-10-symbolic.svg"
	criticalMessage="Your battery power is critical less than ${bat_state} %. If you don't plug in your AC adapter your computer will hibernate soon !"
	criticalIcon="/usr/share/icons/Adwaita/scalable/status/battery-level-0-symbolic.svg"
	
	if [[ "$bat_state" -le "$percentageLow" ]] && [[ ${bat_state} -gt ${percentageCritical} ]]; then 
		/usr/bin/notify-send --urgency=low --icon="${lowIcon}" "${lowMessage}"
	elif [[ ${bat_state} -le ${percentageCritical} ]] && [[ ${bat_state} -gt ${percentageAction} ]]; then 
		/usr/bin/notify-send --urgency=critical --icon="${criticalIcon}" "${criticalMessage}"
	elif [[ ${bat_state} -le ${percentageAction} ]]; then 
        systemctl hybrid-sleep
		exit 0
	fi
fi
exit 0
