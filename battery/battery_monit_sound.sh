#!/bin/bash

ac_state=$(cat /sys/class/power_supply/AC/online)

bat_state=$(cat /sys/class/power_supply/BAT0/capacity)

INTERVAL="60"


if [[ ${ac_state} == 0 ]]; then
	
	percentageLow="7"
	percentageCritical="5"
	percentageAction="2"
	
	lowMessage="Your battery power is low ! less than ${bat_state} %"
	lowIcon="/usr/share/icons/Adwaita/scalable/status/battery-level-10-symbolic.svg"
	criticalMessage="Your battery power is critical less than ${bat_state} %. If you don't plug in your AC adapter your computer will hibernate soon !"
	criticalIcon="/usr/share/icons/Adwaita/scalable/status/battery-level-0-symbolic.svg"
	lowSound="/usr/share/sounds/gnome/default/beeps/negative_beep.ogg"
	
	if [[ "$bat_state" -le "$percentageLow" ]] && [[ ${bat_state} -gt ${percentageCritical} ]]; then 
		/usr/bin/notify-send --urgency=low --expire-time=2000 --icon="${lowIcon}" "${lowMessage}"
	elif [[ ${bat_state} -le ${percentageCritical} ]] && [[ ${bat_state} -gt ${percentageAction} ]]; then 
		/usr/bin/notify-send --urgency=critical --expire-time=5000 --icon="${criticalIcon}" "${criticalMessage}"
		/usr/bin/play $lowSound
	elif [[ ${bat_state} -le ${percentageAction} ]]; then 
        systemctl hybrid-sleep
		exit 0
	fi
elif [[ ${ac_state} == 1 && ${bat_state} == 100 ]]; then

	fullIcon="/usr/share/icons/Adwaita/scalable/status/battery-level-100-symbolic.svg"
	fullMessage="Your battery is charged unplug your charger"
	fullSound="/usr/share/sounds/gnome/default/beeps/beep.ogg	"
	
	/usr/bin/notify-send --urgency=low --expire-time=2000 --icon="${fullIcon}" "${fullMessage}"
	/usr/bin/play $fullSound
fi
exit 0
