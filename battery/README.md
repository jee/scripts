# Battery Monitoring
Because gnome3 power manager is a lttle bit buggy. 
This a little workaround to force my laptop to hibernate instead of running out of battery and lost my unsaved current work ^^ 

## Install

### Copy file to your user systemd directory

```
mkdir -p $HOME/.config/systemd/user
cp $HOME/Scripts/battery_monit.service $HOME/.config/systemd/battery_monit.service
cp $HOME/Scripts/battery_monit.timer $HOME/.config/systemd/battery_monit.timer
```

### systemd timer 

#### Start systemd timer
```
systemctl --user start battery_monit.timer --now

```
#### Enable systemd timer 
```
systemctl --user enable battery_monit.timer

```

#### list user systemd timer
```
systemctl --user list-timers
```
