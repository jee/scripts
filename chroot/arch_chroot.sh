#!/bin/bash

mount /dev/mapper/vgcrypt-arch_root /mnt/arch
mount /dev/nvme0n1p6 /mnt/arch/boot  # boot partition
mount /dev/nvme0n1p1 /mnt/arch/boot/efi

mount -t proc proc /mnt/arch/proc
mount -t sysfs sys /mnt/arch/sys
mount -o bind /dev /mnt/arch/dev
mount -t devpts pts /mnt/arch/dev/pts/
modprobe efivarfs


echo "Now you chroot in /mnt/manjaro"
echo "and 'mount -t efivarfs efivarfs /sys/firmware/efi/efivars'"
echo "if you have error 'EFI variables are not supported on this system.'"

echo "https://gist.github.com/greginvm/af68bef3c81a9594a80d"
