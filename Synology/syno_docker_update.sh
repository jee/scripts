#!/bin/bash

repo_docker_url="https://www.synology.com/en-us/releaseNote/Docker?model=DS218%2B&major=6&minor=2"
docker_update_release_page="/tmp/docker_update_release_page.html"
syno_docker_installed_version=$(synopkg version Docker)
syno_docker_repo_latest_version=$(curl --silent --insecure ${repo_docker_url} > $docker_update_release_page)
latest_v=$(grep "Version:" $docker_update_release_page | head -1 | awk '{print $2}' | sed 's/<\/h4>//g' | sed 's/<h4>//g;')

rm $docker_update_release_page

echo "Current installed docker version is ${syno_docker_installed_version}"
echo "Latest docker released version is $latest_v"
if [ "$syno_docker_installed_version" != "$latest_v" ]; then
  echo "An update for docker is available"
  echo "Check the repo for update"
  echo "$repo_docker_url"
  exit 1
else
  echo "Docker is up to date no action needed"
fi
exit 0
