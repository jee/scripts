#!/bin/bash
# Nom du script .. : mktorrent.sh
# Date ........... : O2.2018
# Auteur ......... : Flip forked from Aerya https://upandclear.org
# Description .... : Dossier/Fichier vers .torrent + taille pieces automatisee
# Prerequis ...... : mktorrent
# Execution ...... : "sh mktorrent.sh ABC"


# Variables ...... : A definir ici et ne pas modifier la suite du script
# TRACKER ........ : URL d'annonce du tracker

RED=""
APL=""
WAFFLES=""
HDO=""
ABN=""
XTHOR=""
PLANETDOC=""

source ~/.config/mktorrent.conf

read -p "Entrer l'adresse du tracker (red/apl/hdo/abn/xthor/waffles/planetdoc) : " TRACKER
    if [ "$TRACKER" = "red" ]; then
        TRACKER="$RED"
    elif [ "$TRACKER" = "apl" ]; then
        TRACKER="$APL"
    elif [ "$TRACKER" = "waffles" ]; then
        TRACKER="$WAFFLES"
    elif [ "$TRACKER" = "hdo" ]; then
        TRACKER="$HDO"
    elif [ "$TRACKER" = "abn" ]; then
        TRACKER="$ABN"
    elif [ "$TRACKER" = "xthor" ]; then
        TRACKER="$XTHOR"
    elif [ "$TRACKER" = "planetdoc" ]; then
        TRACKER="$PLANETDOC"
    fi

# Variables ...... : A ne pas modifier
# TORRENT ........ : Nom du .torrent, d'après celui du Dossier/Fichier cible
# TAILLE ......... : Taille des pieces définie selon Dossier/Fichier (cf https://wiki.vuze.com/w/Torrent_Piece_Size)

TORRENT=$(basename "$1")

read -p "Entrer la taille des pieces (18-25) ou laissez vide pour auto :" PIECE
if [ -z "$PIECE" ]; then
    TAILLE=$(du -s "$1" | awk '{ print $1 }')
    if [ $TAILLE -lt 524288 ]; then
	PIECE=18
    elif [ $TAILLE -lt 1048576 ]; then
	PIECE=19
    elif [ $TAILLE -lt 2097152 ]; then
	PIECE=20
    elif [ $TAILLE -lt 4194304 ]; then
	PIECE=21
    elif [ $TAILLE -lt 8388608 ]; then
	PIECE=22
    elif [ $TAILLE -lt 16777216 ]; then
	PIECE=23
    elif [ $TAILLE -lt 33554432 ]; then
	PIECE=24
    else
	PIECE=25
    fi
fi


# Script ......... : NE PAS MODIFIER
# -p . ........... : private (pas de DHT)
# -l . ........... : length (taille pieces)
# -a . ........... : announce (URL tracker)
# -o . ........... : output (nom du .torrent)
# $1 . ........... : Dossier/Fichier cible

if [ -f /tmp/"$TORRENT".torrent ]; then
    read -p "Ce fichier existe déjà taper yes pour le suprimer ?" DELTHISFILE
    if [ "$DELTHISFILE" = yes ]; then
	rm /tmp/"$TORRENT".torrent
    else 
	echo "Do something !!! Ce fichier existe déjà"
	exit 0
    fi
fi
echo "taille des pieces : $PIECE"
echo "tracker : $TRACKER"
mktorrent -p -l "$PIECE" -a "$TRACKER" -o /tmp/"$TORRENT".torrent "$1"
