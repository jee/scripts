#!/bin/bash
export PATH=$PATH:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
my_dydns="yourdomain.hopto.org"
my_ip=$(host yourdomain.hopto.org | awk '{print $4}')
check_iptables_rules=$(iptables -L -v -n | grep "$my_ip")
check_hosts_deny=$(cat /etc/hosts.deny | grep "$my_ip")
#check_route=$(route | grep "my_ip")
#portsentry_whitelist_ip=`sed -n 25p /etc/portsentry/portsentry.ignore.static`

if [ "$my_ip" != "" ];then
  if [ "$check_iptables_rules" != "" ];then
    iptables -D INPUT -s "$my_ip" -j DROP 
  fi
  if [ "$check_hosts_deny" != "" ];then
    sed -i "/$my_ip/d" /etc/hosts.deny
    route del -host "$my_ip" reject &
  fi
  sed -i "14d;13aALL: $my_ip" /etc/hosts.allow
  service fail2ban restart
elif [ "$my_ip" = "found:" ];then
  echo "$my_dydns Dynamic DNS EXPIRED check https://noip.com " `date` | mail -s 'Dynamic DNS EXPIRED' root
elif [ "$my_ip" = "" ];then
  echo "noip script error" `date` | mail -s 'noip script error' root
  exit;
fi

#if [ "$portsentry_whitelist_ip" != "$my_ip" ];then
#  sed -i "14d;13aALL: $my_ip" /etc/hosts.allow
#  sed -i "25d;24a$my_ip" /etc/portsentry/portsentry.ignore.static
#  service fail2ban restart #&& service portsentry restart
#fi

