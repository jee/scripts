#!/bin/bash

uid=$(id -u)

if [ $uid != 0 ]; then
    echo "Script must be run as root or with sudo"
    exit 1
fi

update_tmp_dir=/tmp/firefox_update_$(date +"%s")
# Get direct url
dl_url=$(curl -L --head -w '%{url_effective}' https://download.mozilla.org/\?product\=firefox-latest-ssl\&os\=linux64\&lang\=en-US 2>/dev/null | tail -n1)
release_filename=${dl_url##*/}

echo  $release_filename

# Donwload Firefox
mkdir $update_tmp_dir

curl -L --output $update_tmp_dir/$release_filename $dl_url
tar -xvf $update_tmp_dir/$release_filename --directory $update_tmp_dir

cp -r $update_tmp_dir/firefox /opt/firefox_new
mv /opt/firefox $update_tmp_dir/firefox_old
mv /opt/firefox_new /opt/firefox

echo "Firefox succefully updated please restart it !!"
