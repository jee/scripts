#!/bin/bash

current_AppImage="/usr/local/bin/nextcloud-desktop"

# Stop running Nextcloud client instance

$current_AppImage --quit

# get latest tag release name

#latest_vtag=$(curl -sX GET "https://api.github.com/repos/nextcloud/desktop/releases/latest" | jq -r .tag_name)
latest_vtag=$(curl --silent "https://api.github.com/repos/nextcloud/desktop/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')
latest_tag=${latest_vtag:1}

AppImageUrl="https://github.com/nextcloud/desktop/releases/download/v$latest_tag/Nextcloud-$latest_tag-x86_64.AppImage"

wget $AppImageUrl --directory-prefix /tmp/

cp /tmp/Nextcloud-$latest_tag-x86_64.AppImage $current_AppImage

$current_AppImage --background &

disown
exit 0
