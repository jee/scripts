#!/bin/bash

nvim_bin="/usr/local/bin/nvim"
nvim_arch="x86_64"
current_AppImage="/usr/local/bin"

# Get Neovim Version

nvim_current_version=$($nvim_bin --version|grep -w 'NVIM'|awk '{print $2}')



# Get latest tag release name
latest_vtag=$(curl --silent "https://api.github.com/repos/neovim/neovim/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')
latest_tag=${latest_vtag:1}

AppImageUrl=https://github.com/neovim/neovim/releases/download/$latest_vtag/nvim.appimage
sha256Url=$AppImageUrl.sha256sum


if [ $nvim_current_version != $latest_vtag ]; then
    echo "A new version of Neovim is available (new: $latest_vtag , current : $nvim_current_version)"
    read -p "Would you update Neovim now (y/n) ?" updateYN
    if [ $updateYN == "y" ]; then
        wget $AppImageUrl -O /tmp/nvim.appimage
        wget $sha256Url -O /tmp/nvim.appimage.sha256
        cd /tmp
        sha256sum -c /tmp/nvim.appimage.sha256 --status
        check_release_status=$?

        if [ $check_release_status == 0 ]; then
            mv $nvim_bin /tmp/nvim_old
            cp /tmp/nvim.appimage $nvim_bin
            chmod +x $nvim_bin
        fi
        nvim_current_version_new=$($nvim_bin --version|grep -w 'NVIM'|awk '{print $2}')

        if [ $nvim_current_version_new == $latest_vtag ]; then
            echo "New version of Neovim ($nvim_current_version_new) is installed"
        else
            echo "Something wrong happened !!!"
            exit 1
        fi
    fi
else 
    echo "Your version of Neovim is up to date (release: $latest_vtag , current : $nvim_current_version)"
fi
exit 0
