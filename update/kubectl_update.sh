#!/bin/bash

# kubectl AppImage Path
k_bin="/mnt/data/bin/kubectl"
k_arch="x86_64"

# Get kubectl Version

k_current_version=$($k_bin version client=true --output='json' | grep '"gitVersion":' | sed -E 's/.*"([^"]+)".*/\1/')
k_release_version=$(curl -L --silent https://dl.k8s.io/release/stable.txt)

current_AppImage="/mnt/data/bin/kubectl"


# get latest tag release name
AppImageUrl=https://dl.k8s.io/release/$k_release_version/bin/linux/amd64/kubectl
sha256Url=$AppImageUrl.sha256


if [ $k_current_version != $k_release_version ]; then
    echo "A new version of kubectl is available (new: $k_release_version , current : $k_current_version)"
    read -p "Would you update kubectl now (y/n) ?" updateYN
    if [ $updateYN == "y" ]; then
        wget $AppImageUrl -O /tmp/kubectl
        wget $sha256Url -O /tmp/kubectl.sha256
        cd /tmp

        #check_sha256sum=$(sha256sum /tmp/kubectl)

        echo "$(<kubectl.sha256) kubectl" > /tmp/kubectl.sha256
        sha256sum -c /tmp/kubectl.sha256 --status
        check_release_status=$?
        if [ $check_release_status == 0 ]; then
            mv $k_bin /tmp/kubectl_old
            cp /tmp/kubectl $k_bin
            chmod +x $k_bin
        fi
        k_current_version_new=$($k_bin version client=true --output='json' | grep '"gitVersion":' | sed -E 's/.*"([^"]+)".*/\1/')

        if [ $k_current_version_new == $k_release_version ]; then
            echo "New version of kubectl ($k_release_version) is installed"
        else
            echo "Something wrong happened !!!"
            exit 1
        fi
    fi
else 
    echo "Your version of Kubectl is up to date (release: $k_release_version , current : $k_current_version)"
fi
exit 0
