#!/bin/bash

# KeePassXC AppImage Path
k_bin="/usr/local/bin/keepassxc"
k_arch="x86_64"

# Get keepassxc Version

k_current_version=$($k_bin --version | awk -F ' ' '{print $2}')
k_release_version=$(curl --silent "https://api.github.com/repos/keepassxreboot/keepassxc/releases/latest" |   grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')


if [ $k_current_version != $k_release_version ]; then
    release_url="https://github.com/keepassxreboot/keepassxc/releases/download/$k_release_version/KeePassXC-$k_release_version-$k_arch.AppImage"
    echo "A new version of KeePassXC is available (new: $k_release_version , current : $k_current_version)"
    read -p "Would you update KeePassXC now (y/n) ?" updateYN
    if [ $updateYN == "y" ]; then
        wget $release_url -O /tmp/KeePassXC-$k_release_version-$k_arch.AppImage
        wget $release_url.DIGEST -O /tmp/KeePassXC-$k_release_version-$k_arch.AppImage.DIGEST
        cd /tmp
        sha256sum -c /tmp/KeePassXC-$k_release_version-$k_arch.AppImage.DIGEST --status
        check_release_status=$?
        if [ $check_release_status == 0 ]; then
            mv $k_bin /tmp/keepassxc_old_$k_current_version.AppImage
            cp /tmp/KeePassXC-$k_release_version-$k_arch.AppImage $k_bin
            chmod +x $k_bin
        fi
        k_current_version_new=$($k_bin --version | awk -F ' ' '{print $2}')

        if [ $k_current_version_new == $k_release_version ]; then
            echo "New version of KeePassXC ($k_release_version) is installed"
        else
            echo "Something wrong happened !!!"
            exit 1
        fi
    fi
else 
    echo "Your version of KeePassXC is up to date (release: $k_release_version , current : $k_current_version)"
fi
exit 0
