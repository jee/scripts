# Docker Scripts

- update-docker-image :
    update recursively docker images

- restart_unhealthy :
   check health status of containers and restart if needed 

## Requirements :

- bash
- docker
- docker-compose
- root access (depending on your system)

## Files structure

Your docker-compose files need to be like this :

```

-/docker
    -/my_app_1
	-/docker-compose
	-/build
	    -/Dockerfile
    -/my_app_2
        -/docker-compose
        -/build
            -/Dockerfile
... etc
```

## Usage

```
sudo ./<Script_name>
```
