import requests
import config
import sys
import xml.etree.ElementTree as ET

#import config

# Livebox
api_url = 'http://192.168.1.1/api/1.0/?method=wan.getInfo'
resp = requests.get(url=api_url)
resp_xml = resp.text

# Parse XML tree to get passowrd
tree = ET.fromstring(resp_xml)

for wan in tree.iter('wan'):
    ipv4_addr = wan.attrib.get('ip_addr')
    ipv6_addr = wan.attrib.get('ipv6_addr')

# Dynu
dynu_url = 'https://api.dynu.com/v2/dns/'
try:
    config.dynu_domain_id
except NameError:
    sys.exit('Please set the dynu_domain_id in the config file')
try:
    config.dynu_api_key
except NameError:
    sys.exit('Please set the dynu_api_key in the config file')

dynu_url = dynu_url + config.dynu_domain_id
dynu_header = {'accept': 'application/json', 'Content-Type': 'application/json', 'API-Key': config.dynu_api_key}
dynu_check_resp = requests.get(url=dynu_url, headers=dynu_header)
dynu_check_data = dynu_check_resp.json()
print(dynu_check_data)

sfr_ipv4 = ipv4_addr
sfr_ipv6 = ipv6_addr
dynu_ipv4 = dynu_check_data['ipv4Address']
dynu_ipv6 = dynu_check_data['ipv6Address']

if ( sfr_ipv4 != dynu_ipv4 ) or ( sfr_ipv6 != dynu_ipv6 ):
    dynu_payload = '{"name":"example.com","group":"","ipv4Address":"'+sfr_ipv4+'","ipv6Address":"'+sfr_ipv6+'","ttl":90,"ipv4":"true","ipv6":"true","ipv4WildcardAlias":"true","ipv6WildcardAlias":"true","allowZoneTransfer":"false","dnssec":"false"}'
    #dynu_payload_data = dynu_payload.json()
    print(dynu_payload)
    print(dynu_url)
    dynu_update_resp = requests.post(url=dynu_url, data=dynu_payload, headers=dynu_header)
    dynu_update_data = dynu_update_resp.json()
    print(dynu_update_data)
else:
    print('IPs are up to date')

