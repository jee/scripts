# get your api token and domin id

## API token

https://www.dynu.com/en-US/ControlPanel/APICredentials

## Domain ID
go to https://www.dynu.com/en-US/Support/API#/dns/dnsGet

click on the autorize button enter your API token .

in the DNS section click on `Try it out` on the get/dns request

or with curl :

```
curl -X GET "https://api.dynu.com/v2/dns" -H  "accept: application/json" -H  "API-Key: YOUR API TOKEN"
```


