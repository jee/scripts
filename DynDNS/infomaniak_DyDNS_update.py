#!/usr/bin/env python3

import argparse
import requests
#import json
#import sys

# Args

parser = argparse.ArgumentParser(prog='infomaniak_DynDNS_update', description='Update Infomaniak DynDNS IP ')
parser.add_argument('--username', '-u', required=True, dest="infomaniak_username", help='Infomaniak DynDNS username')
parser.add_argument('--password', '-p', required=True, dest="infomaniak_password", help="Infomaniak DynDNS password")
parser.add_argument('--hostname', '-hn', required=True, dest="infomaniak_hostname", help="Infomaniak DynDNS hostname")
parser.add_argument('--infomaniakApiUrl', '-i', dest="infomaniakApiUrl", default="https://infomaniak.com/nic/update", help="Infomaniak DynDNS API url (default: https://infomaniak.com/nic/update)")
parser.add_argument('--checkIpUrl', '-c', dest="checkIpUrl", default="https://ifconfig.co/json", help="CheckIP service url (default: ifconfig.co)")

args = parser.parse_args()

# Get public Ip

resp = requests.get(url=args.checkIpUrl)
public_ip = resp.json()['ip']

# Update Infomaniak (doc: https://www.infomaniak.com/fr/support/faq/2376/dyndns-mettre-a-jour-un-dns-dynamique-via-lapi)

try:
    payload = {'username': args.infomaniak_username, 'password': args.infomaniak_password, 'hostname': args.infomaniak_hostname, 'myip': public_ip}
    r = requests.get(args.infomaniakApiUrl, params=payload)
    r.raise_for_status()
except requests.exceptions.HTTPError as err:
    raise SystemExit(err)
