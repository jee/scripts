#!/bin/bash

/usr/bin/wget --quiet https://framagit.org/Shaft/blocage-dmp-unbound/raw/master/arch/adblock-war.conf -O /etc/unbound/adblock-war.conf 
/usr/bin/wget --quiet https://framagit.org/Shaft/blocage-dmp-unbound/raw/master/blocked.zone -O /etc/unbound/blocked.zone

/usr/bin/systemctl restart unbound.service 
exit 0
