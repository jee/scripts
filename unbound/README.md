## /!\ This work only on Arch Linux /!\ 

Auto update Shaft's blocklist for unbound
https://framagit.org/Shaft/blocage-dmp-unbound

# Install
```
sudo cp ~/scripts/unbound/get_unbound_blocklist.{timer,service} /etc/systemd/system/

# Change the script location "ExecStart=/home/jee/scripts/unbound/get_unbound_blocklist.sh" 
# in get_unbound_blocklist.service 
vim /etc/systemd/system/get_unbound_blocklist.service

# Test if it's work
sudo systemctl start get_unbound_blocklist.service
sudo systemctl status get_unbound_blocklist.service

# Enable and start the daily timer
sudo systemctl enable get_unbound_blocklist.timer
sudo systemctl start get_unbound_blocklist.timer

# Verify if the timer is correctly enabled and started
sudo systemctl list-timers --all
```
# Todo

- detect Os and change destination and source files Arch/Debian

---

Thank you Shaft <3
