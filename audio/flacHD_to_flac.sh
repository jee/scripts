#!/bin/bash

if [ -z "$1" ]; then
  
  read -e -i $(echo "$PWD") -p $'Which directory would you transcode to flac (sd) ?\n' PATHTOTRANSCODE
else 
  PATHTOIMPORT="$1"
fi

read -e -i $(echo "48000") -p $'Which Sample Rate (88.2/176.4KHz = 44100Hz | 96/192KHz = 48000Hz)?\n' SAMPLERATE

if [ ! -z "$PATHTOTRANSCODE" ] && [ -d "$PATHTOTRANSCODE" ]; then

  # Remove trailing slash
  PATHTOTRANSCODE="${PATHTOTRANSCODE%/}"
  SOURCEPATH="${PATHTOTRANSCODE%/}"
  OUTPUT_DIR="$PATHTOTRANSCODE"/FLACSD
  
  # Create MP3 directory inside flac directory
  mkdir "$OUTPUT_DIR"

  # Process transcode each .flac files
  for flac_hd_file in "$PATHTOTRANSCODE"/*.flac; do
    flac_sd_file="$OUTPUT_DIR/"$(basename "${flac_hd_file%.*}.flac")

    sox "$flac_hd_file" -G -b 16 "$flac_sd_file" rate -v -L "$SAMPLERATE" dither
    #ffmpeg -i "$flac_file" -codec:a libmp3lame -qscale:a 2 "$flac_sd_file"

  done
  echo "find your transcoded files in $PATHTOTRANSCODE/FLACSD"
  exit 0
else
    echo "Directory $PATHTOTRANSCODE Don't Exist!"
    exit 1
fi
