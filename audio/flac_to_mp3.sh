#!/bin/bash

if [ -z "$1" ]; then
  
  read -e -i $(echo "$PWD") -p $'Which directory would you transcode to mp3 ?\n' PATHTOTRANSCODE
else 
  PATHTOIMPORT=$1
fi

if [ ! -z "$PATHTOTRANSCODE" ] && [ -d "$PATHTOTRANSCODE" ]; then

  # Remove trailing slash
  PATHTOTRANSCODE="${PATHTOTRANSCODE%/}"
  SOURCEPATH="${PATHTOTRANSCODE%/}"
  MP3_OUTPUT_DIR="$PATHTOTRANSCODE"/MP3
  
  # Create MP3 directory inside flac directory
  mkdir "$MP3_OUTPUT_DIR"

  # Process transcode each .flac files
  for flac_file in "$PATHTOTRANSCODE"/*.flac; do
    mp3_file="$MP3_OUTPUT_DIR/"$(basename "${flac_file%.*}.mp3")

    ffmpeg -i "$flac_file" -codec:a libmp3lame -qscale:a 2 "$mp3_file"

  done
  echo "find your transcoded files in $PATHTOTRANSCODE/MP3"
  exit 0
else
    echo "Directory $PATHTOTRANSCODE Don't Exist!"
    exit 1
fi