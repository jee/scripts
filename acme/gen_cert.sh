#!/bin/bash
set -e

# Source config file 
if [ -n "$1" ] || [ -r "$1" ]; then
  source $1
else
  script_full_path=$(readlink -f "$0")
  script_path=`dirname "$script_full_path"`
  conf_path="$script_path/config"
  if [ -n "$conf_path" ] || [ -r "$conf_path" ]; then
    source $conf_path
  else
    echo "No conf File found !"
    #echo "No conf File found !" | mail -s "Backup $BACKUP_DATE" $EMAIL_ADDRESS
    exit1
  fi
fi

source "$script_path"/config

"$ACME_SH_PATH" --upgrade

IFS=',' read -ra MYDOMAIN <<< "$DOMAIN"
for i in "${MYDOMAIN[@]}"; do
    "$ACME_SH_PATH" --issue --keylength 4096 -d "$i" -w "$WEBROOT"
    "$ACME_SH_PATH" --installcert -d "$i" -w "$WEBROOT" \
      --cert-file "$INSTALL_CERT_DIR""$i".cert \
      --key-file "$INSTALL_CERT_DIR""$i"-privkey.key \
      --ca-file "$INSTALL_CERT_DIR""$i"-private.pem \
      --fullchain-file "$INSTALL_CERT_DIR""$i"-fullchain.pem
done
exit 0
