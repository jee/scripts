#!/bin/bash

script_full_path=$(readlink -f "$0") 
script_path=`dirname "$script_full_path"` 

source "$script_path"/config

if ! openssl x509 -checkend 86400 -in "$cer_path" 1> /dev/null
then
	/root/.acme.sh/acme.sh --upgrade
	/root/.acme.sh/acme.sh --renew "$domain" -w "$webroot_dir"
fi
