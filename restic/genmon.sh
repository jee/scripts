#!/bin/bash
#set -e

script_full_path=$(readlink -f "$0")
script_path=`dirname "$script_full_path"`

if [ -n "$1" ] || [ -r "$1" ]; then
  source $1
else
  conf_path="$script_path/config"
  if [ -n "$conf_path" ] || [ -r "$conf_path" ]; then
    source $conf_path
  else
    echo -e "No conf File found !"
    exit 1
  fi
fi
BACKUP_SCRIPT="$script_path/backup.sh"
echo "$script_path"
GREEN_ICON="$script_path/icons/backup_green.png"
YELLOW_ICON="$script_path/icons/backup_yellow.png"
ORANGE_ICON="$script_path/icons/backup_orange.png"
RED_ICON="$script_path/icons/backup_red.png"

/usr/bin/wget -qO- "$IFCONFIG_URL" > /dev/null
if [ $? != "0" ]; then
  echo -e "<click>xfce4-panel --plugin-event=genmon:refresh:bool:true</click>"
  echo -e "<img>$RED_ICON</img>"
  echo -e "<tool><tt><b><u>Error</u></b> : <span foreground='red'>Network connectivity issue</span></tt></tool>"
  echo -e "<tool><small>Click on the icon to refresh</small></tool>"
else
  IFS=',' read -ra BACKUP_ME <<< "$DIR_TO_BACKUP"
  for i in "${BACKUP_ME[@]}"; do
    TMP_NAME_PATH=/tmp/restic_genmon_tmp_${i//\//_}
    if [ ! -f "$TMP_NAME_PATH" ]; then
      RESTIC_SNAPSHOTS_RESULT_JSON=$("$RESTIC_BIN_PATH" --cache-dir "$RESTIC_CACHE_DIR" snapshots --no-lock --last --path "$i" --json)
      RESTIC_SNAPSHOTS_RESULT_JSON_EXIT_CODE="$?"
      if [ "$RESTIC_SNAPSHOTS_RESULT_JSON_EXIT_CODE" != "0" ]; then
	echo -e "<img>$RED_ICON</img>"
	echo -e "<tool><tt><b><u>$i</u></b> : <span foreground='red'>Error : $("$RESTIC_BIN_PATH" --cache-dir "$RESTIC_CACHE_DIR" snapshots --path "$i")</span></tt></tool>"
	exit 1
      fi
      LAST_SNAPSHOT_DATE=$(echo "$RESTIC_SNAPSHOTS_RESULT_JSON" | jq '.[0] | .time')
      echo -e "${LAST_SNAPSHOT_DATE//\"/}" > "$TMP_NAME_PATH"
    fi
    if [ -f "$TMP_NAME_PATH" ]; then
      DIFF_NOW_LAST_SNAPSHOT_DATE=$(( ($(date +%Y%m%d) - $(date --date="$(cat $TMP_NAME_PATH)" +%Y%m%d) ) ))
      LAST_BACKUP_DATE=$(date --date=$(cat $TMP_NAME_PATH) +%Y-%m-%d\ %H:%M:%S)
      if [ "$DIFF_NOW_LAST_SNAPSHOT_DATE" -le "0" ]; then
	echo -e "<click>bash -c 'rm $TMP_NAME_PATH && xfce4-panel --plugin-event=genmon:refresh:bool:true'</click>"
	echo -e "<img>$GREEN_ICON</img>"
	echo -e "<tool><span foreground='green'>$i</span> : $LAST_BACKUP_DATE \n<i><small>Click on the icon to refresh</small></i></tool>"
      elif [ "$DIFF_NOW_LAST_SNAPSHOT_DATE" -eq "1" ]; then
	echo -e "<click>bash -c 'rm $TMP_NAME_PATH && xfce4-panel --plugin-event=genmon:refresh:bool:true'</click>"
	echo -e "<img>$ORANGE_ICON</img>"
	echo -e "<tool><span foreground='orange'>$i</span> : $LAST_BACKUP_DATE \n<i><small>Click on the icon to refresh</small></i></tool>"
      else
	echo -e "<click>bash -c 'rm $TMP_NAME_PATH && xfce4-panel --plugin-event=genmon:refresh:bool:true'</click>"
	echo -e "<img>$RED_ICON</img>"
	echo -e "<tool><tt><b><u>$i</u></b> : <span foreground='red'>$DIFF_NOW_LAST_SNAPSHOT_DATE days ago ($LAST_BACKUP_DATE)</span></tt> \n<i><small>Click on the icon to refresh</small></i></tool>"
      fi
    else
      exit 1
    fi
  done
fi
exit 0
