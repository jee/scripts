#!/bin/bash

set -e

# Source config file 
if [ -n "$1" ] || [ -r "$1" ]; then
  source $1
else
  script_full_path=$(readlink -f "$0")
  script_path=`dirname "$script_full_path"`
  conf_path="$script_path/config"
  if [ -n "$conf_path" ] || [ -r "$conf_path" ]; then
    source $conf_path
  else
    echo "No conf File found !"
    #echo "No conf File found !" | mail -s "Backup $BACKUP_DATE" $EMAIL_ADDRESS
    exit1
  fi
fi

pre_process

post_process

# Prune Repo
echo -e "\n`date` - Running forget and prune...\n"

"$RESTIC_BIN_PATH" --cache-dir "$RESTIC_CACHE_DIR" forget --prune --keep-daily 7 --keep-weekly 4 --keep-monthly 12

echo -e "\n`date` - Backup finished.\n"

exit 0
