#!/bin/bash

set -e

logfile=/tmp/backup.log

init() {
    echo -e "\n`date +%D\ %T` Backup Process Started"
    # array=()

}

config() {
    echo "Get Config File"
    # echo $@
    if [ ! -z $1 ] && [ -r $1 ]; then
        configFile="$1"
        echo -e "Config file found : $(readlink -f $configFile)"
        while read in; do
            if ! [[ "$in" =~ ^# ]]; then
                # index=
                # config+=("$in")
                echo "$in"
            fi
        done < $configFile

        # source $configFile
    else
        script_full_path=$(readlink -f "$0")
        script_path=`dirname "$script_full_path"`
        conf_path="$script_path/config"
        if [ -n "$conf_path" ] || [ -r "$conf_path" ]; then
            source $conf_path
            echo "Config file found" $conf_path
        else
            echo "No conf File found !" $0

            #echo "No conf File found !" | mail -s "Backup $BACKUP_DATE" $EMAIL_ADDRESS
            exit1
        fi
    fi

    return $config
}


backup() {
    # Make Backu
    echo -e "\n`date` - Starting backup...\n"

    IFS=',' read -ra BACKUP_ME <<< "$DIR_TO_BACKUP"
    for i in "${BACKUP_ME[@]}"; do
        "$RESTIC_BIN_PATH" --cache-dir "$RESTIC_CACHE_DIR" backup "$i" $RESTIC_EXTRA_CMD
        "$RESTIC_BIN_PATH" unlock
    done
}

prune() {
    # Prune Repo
    echo -e "\n`date` - Running forget and prune...\n"

    "$RESTIC_BIN_PATH" --cache-dir "$RESTIC_CACHE_DIR" forget --prune --keep-daily 7 --keep-weekly 4 --keep-monthly 12

    "$RESTIC_BIN_PATH" unlock
    echo -e "\n`date` - Backup finished.\n"
}



mailOnFailure() {
    if [ "$1" == 0 ]; then
        echo "No Error"
    else
        echo -e "From: \"sys_notif\" <$smtp_user>" > /tmp/backup_mail.txt
        echo -e "To: \"4mn3zi4\" <$smtp_to>" >> /tmp/backup_mail.txt
        echo -e "Subject: Backup Error" >> /tmp/backup_mail.txt
        cat $logfile >> /tmp/backup_mail.txt

        curl --silent --ssl-reqd \
            --url "smtps://$smtp_server:$smtp_port" \
            --user "$smtp_user:$smtp_pwd" \
            --mail-from "$smtp_user" \
            --mail-rcpt "$smtp_to" \
            --upload-file /tmp/backup_mail.txt

        # tail $logfile | mail -s"Error Backup" 4mn3zi4@protonmail.com
        # cat $logfile
    fi
}

notify() {
    if [ "$1" == 0 ]; then
        notify-send -i face-smiling "Succefully Backup"
    else
        notify-send -i face-crying -u critical -t 30000 -a Restic "Error Backup" "$(tail $logfile)"
    fi
}

main() {
    init
    # config $@
    # echo -e $RESTIC_REPOSITORY
    # pre_process
    backup
    # post_process
    prune
}

echo "Get Config File"
if [ ! -z $1 ] && [ -r $1 ]; then
    configFile="$1"
    echo -e "Config file found : $(readlink -f $configFile)"
    # while read in; do
    #     if ! [[ "$in" =~ ^# ]]; then
    #         echo "$in"
    #         export $in
    #     fi
    # done < $configFile

    source $configFile
else
    script_full_path=$(readlink -f "$0")
    script_path=`dirname "$script_full_path"`
    conf_path="$script_path/config"
    if [ -n "$conf_path" ] || [ -r "$conf_path" ]; then
        source $conf_path
        echo "Config file found" $conf_path
    else
        echo "No conf File found !" $0

        #echo "No conf File found !" | mail -s "Backup $BACKUP_DATE" $EMAIL_ADDRESS
        exit 1
    fi
fi

main $@ |& tee $logfile

exit_code=${PIPESTATUS[0]}

mailOnFailure "$exit_code"
notify "$exit_code"

exit 0
