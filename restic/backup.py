#!/usr/bin/env python3

import subprocess
import configparser
import json
import sys
import datetime
import os
import re
import smtplib
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class Restic:
    def __init__(self):
        self.repository = myconfig.get("restic", "repository")
        self.restic_path = myconfig.get("restic", "path")
        self.restic_cache_dir = myconfig.get("restic", "cache_dir")
        self.restic_dir_to_backup = myconfig.get("restic", "dir_to_backup")
        self.restic_exclude_file = ' ' + myconfig.get("restic", "exclude-file") + ' '
        self.retic_backup_extra_cmd = myconfig.get("restic", "backup_extra_cmd")
        self.restic_env = os.environ.copy()
        self.restic_env["RESTIC_REPOSITORY"] = myconfig.get("restic", "repository")
        self.restic_env["RESTIC_PASSWORD"] = myconfig.get("restic", "password")
        self.restic_env["RESTIC_CACHE_DIR"] = myconfig.get("restic", "cache_dir")
        self.restic_env['PATH'] += ':' + myconfig.get("rclone", "bin_path")
        self.restic_env["RCLONE_CONFIG"] = myconfig.get("rclone", "config_path")

        self.restic_forget = myconfig.get("forget", "enable")
        self.restic_forget_daily = myconfig.get("forget", "daily")
        self.restic_forget_weekly = myconfig.get("forget", "weekly")
        self.restic_forget_monthly = myconfig.get("forget", "monthly")
        self.restic_forget_yearly = myconfig.get("forget", "yearly")

        self.rclone_config = '-o rclone.args="serve restic --stdio --b2-hard-delete --config ' + myconfig.get("rclone", "config_path") + '"'
        self.rclone_path = '-o rclone.program="' + myconfig.get("rclone", "bin_path") + '"'

        self.restic_cmd = self.restic_path + ' ' + self.rclone_path + ' ' + self.rclone_config + ' --cache-dir ' + self.restic_cache_dir + ' '


    def runcommand(self, cmd, job_id):
        proc = subprocess.Popen(cmd,
                            env=self.restic_env,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            shell=True,
                            universal_newlines=True)
        std_out, std_err = proc.communicate()
        print(std_out)
        print(std_err)

        # report = Report()
        # report.onfailure(proc.returncode, std_out, std_err)
        report.log(job_id, proc.returncode, std_out, std_err)

        return proc.returncode, std_out, std_err


    def help(self):
        code, out, err = self.runcommand([self.restic_cmd, '--help']);

    def backup(self):
        for to_backup in json.loads(self.restic_dir_to_backup):
            slugify_id = ''.join(e for e in abc if e.isalnum())
            job_id = 'backup_' + slugify_id
            cmd = self.restic_cmd + ' backup ' + self.retic_backup_extra_cmd + self.restic_exclude_file + ' ' + to_backup
            code, out, err = self.runcommand(cmd, job_id)

    def forget(self):
        cmd = self.restic_cmd + 'forget' + ' --keep-daily ' + self.restic_forget_daily + ' --keep-weekly ' + self.restic_forget_weekly + ' --keep-monthly ' + self.restic_forget_monthly + ' --keep-yearly ' + self.restic_forget_yearly + ' --dry-run'
        code, out, err = self.runcommand(cmd, 'forget')

    def snapshots(self):
        cmd = self.restic_cmd + 'snapshots'
        code, out, err = self.runcommand(cmd, 'snapshots')

class Report:
    def __init__(self):
        self.email = myconfig.get("email", "email")
        self.smtp_server = myconfig.get("email", "smtp_server")
        self.smtp_port = myconfig.get("email", "smtp_port")
        self.smtp_user = myconfig.get("email", "smtp_user")
        self.smtp_password = myconfig.get("email", "smtp_password")
        self.smtp_from_name = myconfig.get("email", "from_name")
        self.smtp_from = myconfig.get("email", "from")
        self.smtp_to = myconfig.get("email", "to")

        self.logfile = myconfig.get("email", "logfile")

        self.message = ''
        self.journal = []

    def log(self, job_id, exit_code, stdout, stderr):
        now = datetime.datetime.now()
        self.journal.append([str(now), job_id, exit_code, str(stderr)])

        if os.path.isfile(self.logfile) or self.logfile != "":
            if exit_code != 0:
                error_code = "ERROR(" + str(exit_code) + ")"
            else:
                error_code = "SUCCESS"

            f = open(self.logfile, "a")
            f.write(str(now) + " [" + error_code + "] " + job_id + " : " + stderr + "\n")
            f.close

        # print(self.journal)

    def onfailure(self):

        self.subject = "Backup Success: "

        for job in self.journal:
            if job[2] != 0:
                self.subject = "Backup Error: " + job[1]

            self.message += str(job) + "\n"

        self.sendMail(self.subject, self.message)

    def sendMail(self, subject, message):
        msg = MIMEMultipart()
        msg['From'] = '"' + self.smtp_from_name + '"' + ' <' + self.smtp_from + '>'
        msg['To'] = self.smtp_to
        msg['Subject'] = subject
        message = message
        msg.attach(MIMEText(message))

        mailserver = smtplib.SMTP_SSL(self.smtp_server, self.smtp_port)
        mailserver.ehlo()
        # mailserver.starttls()
        mailserver.ehlo()
        mailserver.login(self.smtp_user, self.smtp_password)

        try:
            mailserver.sendmail(self.smtp_from, self.smtp_to, msg.as_string())
        except smtplib.SMTPException as e:
            print(e)

        mailserver.quit()


def main():
    if len(sys.argv) > 2:
        print(help())
        sys.exit("too many arguments")
    if len(sys.argv) == 2 and os.path.isfile(sys.argv[1]):
        configFile = sys.argv[1]
    else:
        configFile = os.path.realpath(os.path.dirname(sys.argv[0])) + 'config.ini'
        if not os.path.isfile(configFile):
            print("Error no config file found")
            sys.exit(1)

    global myconfig
    global report
    global restic

    # Get Config
    myconfig = configparser.ConfigParser()
    myconfig.read(configFile)

    report = Report()
    restic = Restic()
    # restic.help()
    # giveMe = restic.giveMe()
    restic.snapshots()
    restic.snapshots()

    # restic.backup()
    #restic.forget()

    report.onfailure()


    sys.exit(0)

main()
