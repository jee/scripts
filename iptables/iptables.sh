#!/bin/bash

### BEGIN INIT INFO
# Provides:          wonderwall
# Required-Start:    $remote_fs $local_fs $syslog $network
# Required-Stop:     $remote_fs $local_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: wonderwall initscript
# Description:       Custom Firewall
### END INIT INFO

##################################
#           WONDERWALL           #
##################################
echo '[INFO] Wonderwall, a custom firewall using iptables rules.'

##### VARIABLES #####
script_full_path=$(readlink -f "$0")
script_path=`dirname "$script_full_path"`
source "$script_path"/config

# Flush chains #
/sbin/iptables -F INPUT
/sbin/iptables -F OUTPUT
/sbin/iptables -F LIMIT_ICMP
/sbin/iptables -F TCP_CHECK
/sbin/iptables -F PORTSCAN
/sbin/iptables -F DSHIELD
/sbin/iptables -F SPAMHAUS
/sbin/iptables -F SPOOFED
/sbin/iptables -F SYN-FLOOD
/sbin/iptables -F HTTP-FLOOD
/sbin/iptables -F NEEDS
echo '[OK] Flushed rules and custom chains.'

##### Base #####
# iptables policy #
/sbin/iptables -P INPUT DROP
/sbin/iptables -P FORWARD DROP
/sbin/iptables -P OUTPUT ACCEPT
echo '[OK] Policy set.'

# Enable iPV4 forwarding #
echo "1" > /proc/sys/net/ipv4/ip_forward
echo '[OK] Forwarding iPV4.'

# Allow local connections #
/sbin/iptables -A INPUT -i lo -j ACCEPT
echo '[OK] local allowed.'

# Allow Ping (ICMP) && prevent ping flood #
/sbin/iptables -N LIMIT_ICMP
/sbin/iptables -A LIMIT_ICMP -p icmp -m limit --limit 2/s -j ACCEPT
/sbin/iptables -A LIMIT_ICMP -p icmp -j DROP
/sbin/iptables -A INPUT -p icmp --icmp-type echo-request -j LIMIT_ICMP
echo '[OK] Ping (icmp) allowed + preventing ping flood.'

# Allow etablished connections #
/sbin/iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
echo '[OK] Etablished connections allowed.'

#### Docker ####
if [ -x /usr/bin/docker ]
then
  # Create Docker Table
  /sbin/iptables -t filter -N DOCKER
  /sbin/iptables -t filter -N DOCKER-ISOLATION
  echo '[OK] Table Docker created'

  # Allow docker0 connections #
  /sbin/iptables -A INPUT -i docker0 -j ACCEPT
  /sbin/iptables -A FORWARD -i docker0 -o "$NETWORK_INTERFACE" -j ACCEPT
  /sbin/iptables -A FORWARD -i "$NETWORK_INTERFACE" -o docker0 -j ACCEPT
  echo '[OK] docker0 allowed'
fi

#### Security ####
# TCP packets check #
/sbin/iptables -N TCP_CHECK
/sbin/iptables -I INPUT -i "$NETWORK_INTERFACE" -j TCP_CHECK
/sbin/iptables -D FORWARD -i "$NETWORK_INTERFACE" -j TCP_CHECK
/sbin/iptables -I FORWARD -i "$NETWORK_INTERFACE" -j TCP_CHECK
/sbin/iptables -A TCP_CHECK -p tcp ! --tcp-flags all syn -m state --state NEW -j DROP
/sbin/iptables -A TCP_CHECK -f -j DROP
echo '[OK] TCP packet verification.'

# Spoofing protection #
/sbin/iptables -N SPOOFED
/sbin/iptables -I INPUT -i "$NETWORK_INTERFACE" -j SPOOFED
/sbin/iptables -D FORWARD -i "$NETWORK_INTERFACE" -j SPOOFED
/sbin/iptables -I FORWARD -i "$NETWORK_INTERFACE" -j SPOOFED
/sbin/iptables -A SPOOFED -s 127.0.0.0/8 -j DROP
/sbin/iptables -A SPOOFED -s 169.254.0.0/16 -j DROP
/sbin/iptables -A SPOOFED -s 172.16.0.0/12 -j DROP
/sbin/iptables -A SPOOFED -s 192.0.2.0/24 -j DROP
/sbin/iptables -A SPOOFED -s 192.168.0.0/16 -j DROP
/sbin/iptables -A SPOOFED -s 10.0.0.0/8 -j DROP
/sbin/iptables -A SPOOFED -s 224.0.0.1/4 -j DROP
/sbin/iptables -A SPOOFED -s 240.0.0.0/4 -j DROP
echo '[OK] Spoofing protection.'

# Basic scan protection #
/sbin/iptables -N PORTSCAN
/sbin/iptables -I INPUT -i "$NETWORK_INTERFACE" -p tcp -j PORTSCAN
/sbin/iptables -D FORWARD -i "$NETWORK_INTERFACE" -p tcp -j PORTSCAN
/sbin/iptables -I FORWARD -i "$NETWORK_INTERFACE" -p tcp -j PORTSCAN
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags ACK,FIN FIN -j DROP
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags ACK,PSH PSH -j DROP
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags ACK,URG URG -j DROP
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags ALL ALL -j DROP
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags ALL NONE -j DROP
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j DROP
/sbin/iptables -A PORTSCAN -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP
echo '[OK] Basic scan protection.'

# Syn-flood protection #
/sbin/iptables -N SYN-FLOOD
/sbin/iptables -I INPUT -i "$NETWORK_INTERFACE" -p tcp --syn -j SYN-FLOOD
/sbin/iptables -D FORWARD -i "$NETWORK_INTERFACE" -p tcp --syn -j SYN-FLOOD
/sbin/iptables -I FORWARD -i "$NETWORK_INTERFACE" -p tcp --syn -j SYN-FLOOD
/sbin/iptables -A SYN-FLOOD -m limit --limit 25/s --limit-burst 50 -j RETURN
/sbin/iptables -A SYN-FLOOD -j LOG --log-prefix "SYN-FLOOD "
/sbin/iptables -A SYN-FLOOD -j DROP
echo '[OK] Syn-flood protection.'

# Http flood protection (bye bixxi) #
/sbin/iptables -N HTTP-FLOOD
/sbin/iptables -D INPUT -i "$NETWORK_INTERFACE" -p tcp --syn --dport 80 -m connlimit --connlimit-above 1 -j HTTP-FLOOD
/sbin/iptables -D INPUT -i "$NETWORK_INTERFACE" -p tcp --syn --dport 443 -m connlimit --connlimit-above 1 -j HTTP-FLOOD
/sbin/iptables -I INPUT -i "$NETWORK_INTERFACE" -p tcp --syn --dport 80 -m connlimit --connlimit-above 1 -j HTTP-FLOOD
/sbin/iptables -I INPUT -i "$NETWORK_INTERFACE" -p tcp --syn --dport 443 -m connlimit --connlimit-above 1 -j HTTP-FLOOD
/sbin/iptables -A HTTP-FLOOD -m limit --limit 10/s --limit-burst 10 -j RETURN
/sbin/iptables -A HTTP-FLOOD -m limit --limit 1/s --limit-burst 10 -j LOG --log-prefix "HTTP-FLOOD "
/sbin/iptables -A HTTP-FLOOD -j DROP
echo '[OK] Http-flood protection.'

# Blacklist SPAMHAUS (external script must be executed) #
/sbin/iptables -N SPAMHAUS
/sbin/iptables -I INPUT -i "$NETWORK_INTERFACE" -j SPAMHAUS
/sbin/iptables -D FORWARD -i "$NETWORK_INTERFACE" -j SPAMHAUS
/sbin/iptables -I FORWARD -i "$NETWORK_INTERFACE" -j SPAMHAUS
echo '[OK] Added DSHIELD chain (waiting for allowed HTTP/DNS).'

# Blacklist DSHIELD (external script must be executed) #
/sbin/iptables -N DSHIELD
/sbin/iptables -I INPUT -i "$NETWORK_INTERFACE" -j DSHIELD
/sbin/iptables -D FORWARD -i "$NETWORK_INTERFACE" -j DSHIELD
/sbin/iptables -I FORWARD -i "$NETWORK_INTERFACE" -j DSHIELD
echo '[OK] Added DSHIELD chain (waiting for allowed HTTP/DNS).'

# Perm Ban List
if [ ! -z "$PERM_BAN_LIST" ] && [ -r "$PERM_BAN_LIST" ]; then
  PERM_BAN_CHAIN="PERM_BAN"

  /sbin/iptables -N $PERM_BAN_CHAIN
  /sbin/iptables -F $PERM_BAN_CHAIN

  perm_ban_ip_list=$( cat $PERM_BAN_LIST | awk '/^[0-9]/' | sort -n)

  for IP in $perm_ban_ip_list; do
    /sbin/iptables -A $PERM_BAN_CHAIN -p 0 -s $IP -j DROP
  done
  echo "[OK] Added $PERM_BAN_CHAIN chain with droped IPs from $PERM_BAN_LIST"
else
  echo "[NO] $PERM_BAN_CHAIN Not Added"
fi

##### SPECIFIC #####

# NEEDS Chain #
/sbin/iptables -t filter -N NEEDS
/sbin/iptables -N NEEDS
/sbin/iptables -A INPUT -j NEEDS

# Allow SSH/SCP #
/sbin/iptables -A NEEDS -p tcp --dport "$SSH" -j ACCEPT
echo "[OK] SSH/SCP allowed. ($SSH)"

# Allow HTTP #
if [ ! -z $HTTP ] && [[ ! $HTTP =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  /sbin/iptables -A NEEDS -p tcp --dport "$HTTP" -j ACCEPT
  echo "[OK] HTTP allowed. ($HTTP)"
fi
# Allow HTTPS #
if [ ! -z $HTTPS ] && [[ ! $HTTPS =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  /sbin/iptables -A NEEDS -p tcp --dport "$HTTPS" -j ACCEPT
  echo "[OK] HTTP allowed. ($HTTPS)"
fi

# Allow DNS #
if [ ! -z $DNS ] && [[ ! $DNS =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  /sbin/iptables -A NEEDS -p tcp --dport "$DNS" -j ACCEPT
  /sbin/iptables -A NEEDS -p udp --dport "$DNS" -j ACCEPT
  echo "[OK] DNS allowed. ($DNS udp/tcp)"
fi

# Allow SMTP #
if [ ! -z $SMTP ] && [[ ! $SMTP =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  iptables -A NEEDS -p tcp -s localhost --dport 587 -j ACCEPT
  iptables -A NEEDS -p tcp -s localhost --sport 587 -j ACCEPT
  echo "[OK] SMTP allowed (localhost only). (587)"
  iptables -A NEEDS -p tcp -s localhost --dport 25 -j ACCEPT
  iptables -A NEEDS -p tcp -s localhost --sport 25 -j ACCEPT
  echo "[OK] SMTP allowed (localhost only). (25)"
fi
# Allow VPN #
if [ ! -z $VPNUDP ] && [[ ! $VPNUDP =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  iptables -A NEEDS -p udp -s localhost --dport "$VPNUDP" -j ACCEPT
  echo "[OK] VPN allowed. ($VPNUDP udp)"
fi
if [ ! -z $VPNTCP ] && [[ ! $VPNTCP =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  iptables -A NEEDS -p tcp -s localhost --dport "$VPNTCP" -j ACCEPT
  echo "[OK] VPN allowed. ($VPNTCP tcp)"
fi

# Allow NetData #
if [ ! -z $NETDATA ] && [[ ! $NETDATA  =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  /sbin/iptables -A NEEDS -p tcp --dport "$NETDATA" -j ACCEPT
  echo "[OK] NETDATA allowed. ($NETDATA)"
fi

# Allow Deluge #
if [ ! -z $DELUGE ] && [[ ! $DELUGE  =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  /sbin/iptables -A NEEDS -p tcp --dport "$DELUGE" -j ACCEPT
  echo "[OK] DELUGE allowed. ($DELUGE)"
fi
if [ ! -z $DELUGEWEB ] && [[ ! $DELUGEWEB  =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  /sbin/iptables -A NEEDS -p tcp --dport "$DELUGEWEB" -j ACCEPT
  echo "[OK] DELUGEWEB allowed. ($DELUGEWEB)"
fi

# Allow PLEX #
if [ ! -z $PLEX ] && [[ ! $PLEX  =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  /sbin/iptables -A NEEDS -p tcp --dport "$PLEX" -j ACCEPT
  /sbin/iptables -A NEEDS -p udp --dport "$PLEX" -j ACCEPT
  echo "[OK] PLEX allowed. ($PLEX udp/tcp)"
fi

# Solr only localhost #
if [ ! -z $SOLR ] && [[ ! $SOLR  =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  /sbin/iptables -A INPUT -p tcp -s localhost --dport "$SOLR"  -j ACCEPT
  /sbin/iptables -A INPUT -p tcp --dport "$SOLR" -j DROP
  echo "[OK] Solr allowed ( $SOLR only localhost)"
fi

# Allow IPFS # 
if [ ! -z $IPFS ] && [[ ! $IPFS  =~ ^[fF][aA][lL][sS][eE]$ ]]; then
  /sbin/iptables -A NEEDS -p tcp --dport "$IPFS" -j ACCEPT
  /sbin/iptables -A NEEDS -p udp --dport "$IPFS" -j ACCEPT 
  echo "[OK] IPFS allowed. ($IPFS udp/tcp)"
fi

# CALL EXTERNAL SCRIPTS #
"$script_path"/dshield.sh
echo '-> Downloaded & applied DSHIELD blacklist (up to date).'
"$script_path"/spamhaus.sh
echo '-> Downloaded & applied SPAMHAUS blacklist (up to date).'

##### END #####
echo '[INFO] Firewall configuration is done.'
echo '[INFO] Use "iptables -nvL to display your configuration.'
