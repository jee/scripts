#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/scripts

URL="http://feeds.dshield.org/block.txt"
FILE="/tmp/dshield_block.text"
CHAIN="DSHIELD"

iptables -F $CHAIN

wget -qc $URL -O $FILE
blocklist=$( cat $FILE | awk '/^[0-9]/' | awk '{print $1"/"$3}'| sort -n)

for IP in $blocklist; do
    iptables -A $CHAIN -p 0 -s $IP -j DROP
done

unlink $FILE
exit 0
