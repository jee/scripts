#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/scripts

URL1="http://www.spamhaus.org/drop/drop.lasso"
URL2="http://www.spamhaus.org/drop/edrop.lasso"
FILE1="/tmp/drop.lasso"
FILE2="/tmp/edrop.lasso"
CHAIN="SPAMHAUS"

iptables -F $CHAIN

for bl in 1 2; do
    URL="URL${bl}"
    URL="${!URL}"
    FILE="FILE${bl}"
    FILE="${!FILE}"

    wget -qc ${URL} -O ${FILE}

    for IP in $( cat $FILE | egrep -v '^\s*;' | awk '{ print $1}' ); do
        iptables -A $CHAIN -p 0 -s $IP -j DROP
    done

    unlink ${FILE}
done

exit 0
