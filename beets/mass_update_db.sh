#!/bin/bash
BEETSDIR=/config
export BEETSDIR
FPCALC=/usr/bin/fpcalc
export FPCALC

for Directory in "$1"*; do
    if [ -d "${Directory}" ]; then
       /usr/local/bin/beet -c /config/config.yaml import --nocopy --nowrite --noautotag --flat "${Directory}"
    fi
done

