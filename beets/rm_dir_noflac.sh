#!/bin/bash

BEETSDIR=/config
export BEETSDIR
FPCALC=/usr/bin/fpcalc
export FPCALC

if [ -z $1 ]; then
  SOURCEPATH="/Media/Music/Unsorted"
else
  SOURCEPATH="$1"
  SOURCEPATH="${SOURCEPATH%/}"
fi

for Directory in "$SOURCEPATH"/*; do
    if [ -d "${Directory}" ]; then
      #COUNT_FLAC=$(ls -1q "${Directory}"/*.flac | wc -l)
      COUNT_FLAC=$(find "${Directory}" -type f -name "*.flac" | wc -l)
      COUNT_DIR=$(find "${Directory}"/* -maxdepth 0 -type d | wc -l)
      ALBUM_NAME=$(basename "${Directory}")
      if [ "$COUNT_FLAC" == 0 ]; then
        echo -e "$ALBUM_NAME will be removed it contains :"
        echo -e "$COUNT_FLAC flac files \t $COUNT_DIR directory"
        rm --recursive --force "${Directory}"
      fi
    fi
done

exit 0
