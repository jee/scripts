#!/bin/bash

beets_dir="/home/torrent/.config/beets"
beets_config="/home/torrent/.config/beets/config.yaml"
watch_dir="/mnt/data/torrents/seed/Music"

inotifywait -m -e create,moved_to --format '%w%f' "$watch_dir" | while IFS= read -r dir_path; do
    /usr/local/bin/beet -c "$beets_config" import -q -i "$dir_path"
done
