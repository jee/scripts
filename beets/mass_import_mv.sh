#!/bin/bash
BEETSDIR=/config
export BEETSDIR
FPCALC=/usr/bin/fpcalc
export FPCALC

for Directory in "$1"*; do
    if [ -d "${Directory}" ]; then
       /usr/local/bin/beet -c /config/config.yaml import --move --quiet --incremental --flat "${Directory}"
    fi
done

