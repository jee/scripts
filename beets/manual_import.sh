#!/bin/bash

beets_bin="/usr/bin"
beets_dir="/config/beets"
beets_config="/config/config.yaml"

BEETSDIR=/config
export BEETSDIR
FPCALC=/usr/bin/fpcalc
export FPCALC

if [ -z "$1" ]; then
  read -e -i '/Media/Music/FLAC/' -p $'Which directory would you (re)import ?\n' PATHTOIMPORT
  if [ ! -z "$PATHTOIMPORT" ] && [ -d "$PATHTOIMPORT" ]; then
	# Remove trailing slash
  	PATHTOIMPORT="${PATHTOIMPORT%/}"
    SOURCEPATH="${PATHTOIMPORT%/}"
  	COUNT_FLAC_1=$(ls -1q "$PATHTOIMPORT"/*.flac | wc -l)
    echo "$COUNT_FLAC_1"
    if [ "$COUNT_FLAC_1" == 1 ] && [ -r "$PATHTOIMPORT"/*.cue ]; then
      echo "Your directory/album contain only 1 flac file and a cue file"
      read -e -p $'Would try to split it ?(y/n)' SPLIT_YN
      if [ "$SPLIT_YN" = "y" ]; then
        mkdir "$PATHTOIMPORT"/split
        cp "$PATHTOIMPORT"/*.{jpg,jpeg,png,cue,log} "$PATHTOIMPORT"/split/
        /usr/bin/shntool split -f "$PATHTOIMPORT"/*.cue -o flac -O never -t "%n. %t - %p - %a" -d "$PATHTOIMPORT"/split "$PATHTOIMPORT"/*.flac
        PATHTOIMPORT="$PATHTOIMPORT"/split
      fi
    fi
    /usr/bin/beet -c "$beets_config" import --flat "$PATHTOIMPORT" 
    #rm -r "$PATHTOIMPORT_SPLIT" "$PATHTOIMPORT2_SPLIT"
    read -e -p $'Would remove source directory ($SOURCEPATH) ?(yes/n)' RM_SOURCE_DIR_YN
    if [ "$RM_SOURCE_DIR_YN" = "yes" ]; then
      rm -rf "$SOURCEPATH"
    fi
  else
    echo "Directory $PATHTOIMPORT Don't Exist!"
    exit 1
  fi
else
  if [ ! -z "$1" ] && [ -d "$1" ]; then
    PATHTOIMPORT="${1%/}"
    COUNT_FLAC_1=$(ls -1q "$PATHTOIMPORT"/*.flac | wc -l)
    echo "$COUNT_FLAC_1"
    if [ "$COUNT_FLAC_1" == 1 ] && [ -r "$PATHTOIMPORT"/*.cue ]; then
      mkdir "$PATHTOIMPORT"/split
      cp "$PATHTOIMPORT"/*.{jpg,jpeg,png,cue,log} "$PATHTOIMPORT"/split/
      /usr/bin/shntool split -f "$PATHTOIMPORT"/*.cue -o flac -O never -t "%n. %t" -d "$PATHTOIMPORT"/split "$PATHTOIMPORT"/*.flac
      PATHTOIMPORT_SPLIT="$PATHTOIMPORT"/split
      PATHTOIMPORT="$PATHTOIMPORT"/split
	fi
	/usr/bin/beet -c "$beets_config" import --flat "$PATHTOIMPORT" 
    rm -r "$PATHTOIMPORT_SPLIT"
  else
    echo "Directory $1 Don't Exist!"
    exit 1
  fi
fi
