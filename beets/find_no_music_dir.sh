#!/bin/bash


root_dir_to_scan="$1"


find $root_dir_to_scan -mindepth 0 -maxdepth 1 -type d -exec sh -c 'ls -1 "{}"|egrep -i -q "^*\.(MP3|mp3)$"' ';' \
  | sort -r | awk 'a!~"^"$0{a=$0;print}' | sort
