#!/bin/bash

beets_dir="/config/beets"
beets_config="/config/config.yaml"
#watch_dir="/torrents/seed/Music"
fallback_dir="/Media/Music/Unsorted"
log_file="/config/beets_flac.log"
BEETSDIR=/config
export BEETSDIR
FPCALC=/usr/bin/fpcalc
export FPCALC

inotifywait -m -e create,moved_to --format '%w%f' "$WATCH_DIR" | while IFS= read -r dir_path; do
  dir_path_clean="${dir_path%/}"
  folder_name=$(basename "$dir_path_clean")
  cp -r "$dir_path_clean" "$fallback_dir"/
  dir_path="$fallback_dir/$folder_name"
  if [ ! -z "$dir_path" ] && [ -d "$dir_path" ]; then
    PATHTOIMPORT="${dir_path%/}"
    COUNT_FLAC_1=$(ls -1q "$PATHTOIMPORT"/*.flac | wc -l)
    if [ "$COUNT_FLAC_1" == 1 ] && [ -r "$PATHTOIMPORT"/*.cue ]; then
      mkdir "$PATHTOIMPORT"/split
      cp "$PATHTOIMPORT"/*.{jpg,jpeg,png,cue,log} "$PATHTOIMPORT"/split/ 2>/dev/null
      /usr/bin/shntool split -f "$PATHTOIMPORT"/*.cue -o flac -O never -t "%n. %t" -d "$PATHTOIMPORT"/split "$PATHTOIMPORT"/*.flac
      PATHTOIMPORT_SPLIT="$PATHTOIMPORT"/split
      PATHTOIMPORT="$PATHTOIMPORT"/split
	fi
    /usr/local/bin/beet -c "$beets_config" import --move --quiet --incremental --flat "$PATHTOIMPORT"
    rm -rf "$PATHTOIMPORT_SPLIT" 2>/dev/null
    COUNT_FLAC_2=$(ls -1q "$PATHTOIMPORT"/*.flac | wc -l)
    if [ "$COUNT_FLAC_2" == 0 ]; then
      rm -rf "$PATHTOIMPORT" 2>/dev/null
      echo "RMRF $PATHTOIMPORT"
      echo "RMRF $PATHTOIMPORT" >> $log_file
    else
      echo "WARN $PATHTOIMPORT"
      echo "WARN $PATHTOIMPORT" >> $log_file
      echo "There are still audio files in this directory please delete it manualy"
      echo "There are still audio files in this directory please delete it manualy" >> $log_file
    fi
  fi
done
