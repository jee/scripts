#!/bin/bash

beets_dir="/config/beets"
beets_config="/config/config.yaml"

BEETSDIR=/config
export BEETSDIR
FPCALC=/usr/bin/fpcalc
export FPCALC
/usr/bin/beet $@
exit 0
