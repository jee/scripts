#!/bin/bash

set -e

if [ -n "$1" ] || [ -r "$1" ]; then
  source $1
else
  script_full_path=$(readlink -f "$0")
  script_path=`dirname "$script_full_path"`
  conf_path="$script_path/config"
  if [ -n "$conf_path" ] || [ -r "$conf_path" ]; then
    source $conf_path
  else
    echo "No conf File found !"
    echo "No conf File found !" | mail -s "Backup $BACKUP_DATE" $EMAIL_ADDRESS
    exit1
  fi
fi


cleanup()
{
  echo "Something bad happened during backup, check ${LOG_FILE}" >> ${TMP_LOG_FILE}
  cat "$TMP_LOG_FILE" | mail -s "Backup $BACKUP_DATE" $EMAIL_ADDRESS
  cat $TMP_LOG_FILE >> $LOG_FILE
  #rm $TMP_LOG_FILE
  #rm $TMP_DPKG_LIST_FILE
  #rm $TMP_MYSQL_DUMP_FILE
  if [[ $(findmnt -M "$FTP_MOUNT_POINT") ]]; then
    umount $FTP_MOUNT_POINT
  fi
  exit $1
}

ts_log()
{
    echo `date '+%Y-%m-%d %H:%m:%S'` $1 >> ${TMP_LOG_FILE}
}

mount_ftp () {

  if [[ $(findmnt -M "$FTP_MOUNT_POINT") ]]; then
    umount $FTP_MOUNT_POINT
    curlftpfs -v $FTP_HOST $FTP_MOUNT_POINT -o user=$FTP_USER:$FTP_PASSWORD -o allow_root
  else 
    curlftpfs -v $FTP_HOST $FTP_MOUNT_POINT -o user=$FTP_USER:$FTP_PASSWORD -o allow_root
  fi
}

unmout_ftp () {
  umount $FTP_MOUNT_POINT
}

sync_local_to_remote () {
  rsync --archive --itemize-changes --temp-dir=/mnt/data/backup/tmp --exclude=keepall --delete-before --no-perms --checksum $BORG_REPOSITORY/ $FTP_MOUNT_POINT >> ${TMP_LOG_FILE} 2>&1
}

# Trap on non-zero exit
trap '[ "$?" -eq 0 ] || cleanup' EXIT

if [ ! -z "$REMOTE_SYNC" ]; then
  case "$REMOTE_SYNC" in
    "ftp"|"FTP")
      ts_log "Mount FTP..."
      mount_ftp
      ts_log "Sync local to remote..."
      sync_local_to_remote
      ts_log "Unmount FTP..."
      unmout_ftp
      ;;
    "b2"|"B2")
      ts_log "Sync local to B2..."
      /usr/bin/rclone --verbose --log-file "${TMP_LOG_FILE}" --config "$RCLONE_CONFIG" sync --transfers 32 "$BORG_REPOSITORY" "$RCLONE_B2_REMOTE":/
      ;;
    *)
      ts_log "Error: Remote Sync Storage no recognized"
      ;;
  esac
else 
  ts_log "No Remote Sync"
fi
ts_log 'Send mail report...'
ts_log 'Cleaning up temp files...'
cat "$TMP_LOG_FILE" | mail -s "Backup $BACKUP_DATE" "$EMAIL_ADDRESS"
cat $TMP_LOG_FILE >> $LOG_FILE
rm $TMP_LOG_FILE 

if [ "$BACKUP_PKG_LIST" = "true" ]; then
  rm $TMP_DPKG_LIST_FILE
fi
if [ "$BACKUP_MYSQL" = "true" ]; then
  rm $TMP_MYSQL_DUMP_FILE
fi
if [ "$BACKUP_POSTGRESQL" = "true" ]; then
  rm $TMP_PSQL_DUMP_FILE
fi
if [ "$BACKUP_LDAP" = "true" ]; then
  rm $TMP_LDAP_DUMP_FILE
fi
