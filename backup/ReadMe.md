pre & post script for [borgmatic](https://torsion.org/borgmatic/)

```
hooks:
    # List of one or more shell commands or scripts to execute before creating a backup.
    before_backup:
        - echo "`date` - Starting a backup job."
        - /root/scripts/backup/pre_backup.sh 
    # List of one or more shell commands or scripts to execute after creating a backup.
    after_backup:
        - /root/scripts/backup/post_backup.sh
        - echo "`date` - Backup created."

    # List of one or more shell commands or scripts to execute in case an exception has occurred.
    on_error:
        - echo "`date` - Error while creating a backup."
```
