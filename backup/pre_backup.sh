#!/bin/bash

set -e

if [ -n "$1" ] || [ -r "$1" ]; then
  source $1
else
  script_full_path=$(readlink -f "$0")
  script_path=`dirname "$script_full_path"`
  conf_path="$script_path/config"
  if [ -n "$conf_path" ] || [ -r "$conf_path" ]; then
    source $conf_path
  else
    echo "No conf File found !"
    echo "No conf File found !" | mail -s "Backup $BACKUP_DATE" $EMAIL_ADDRESS
    exit1
  fi
fi


cleanup()
{
    echo "Something bad happened during backup, check ${TMP_LOG_FILE}"
    ts_log 'Error: Something bad happened during backup'
    cat "$TMP_LOG_FILE" | mail -s "Error Backup $BACKUP_DATE" $EMAIL_ADDRESS
    cat $TMP_LOG_FILE >> $LOG_FILE
    #rm $TMP_LOG_FILE
    #rm $TMP_DPKG_LIST_FILE
    #rm $TMP_MYSQL_DUMP_FILE
    exit $1
}

ts_log()
{
    echo `date '+%Y-%m-%d %H:%m:%S'` $1 >> ${TMP_LOG_FILE}
}

# Trap on non-zero exit
trap '[ "$?" -eq 0 ] || cleanup' EXIT

ts_log "Starting new backup ${BACKUP_DATE}..."

if [ "$BACKUP_MYSQL" = "true" ]; then
  ts_log 'Dumping MySQL db...'
  mysqldump --all-databases --events -p$MYSQL_ROOT_PASS > $TMP_MYSQL_DUMP_FILE
fi

if [ "$BACKUP_POSTGRESQL" = "true" ]; then
  ts_log 'Dumping PostgreSQL db...'
  pg_dumpall > $TMP_PSQL_DUMP_FILE
fi
if [ "$BACKUP_PKG_LIST" = "true" ]; then
  ts_log 'dpkg list intalled packages'
  /usr/bin/dpkg --get-selections > $TMP_DPKG_LIST_FILE
fi
if [ "$BACKUP_LDAP" = "true" ]; then
  ts_log 'Dumping LDAP...'
  slapcat -l $TMP_LDAP_DUMP_FILE
fi
