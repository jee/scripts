#!/bin/bash

script_full_path=$(readlink -f "$0") 
script_path=`dirname "$script_full_path"` 
filename=$(basename "$file")
source "$script_path"/config


func_watch () {
  inotifywait -m \
  -e create \
  --format '%w%f' \
  --excludei '(.gz$|.log$)' \
  "$watch_dir"
}

func_encrypt_log () {
	gpg --output /tmp/$(basename $file)_gpg-encrypt --encrypt --recipient "$gpg_recipient" "$file"
}

func_send_mail () {
	echo 'find the log file in attachement' | mutt -s $(basename $file) -a /tmp/$(basename $file)_gpg-encrypt -- "$mail_recipient"
}

func_clean_up_tmp_file () {
	rm -rf /tmp/$(basename $file)_gpg-encrypt
}


func_watch | \
while IFS= read -r file; do
  func_encrypt_log
  func_send_mail
  func_clean_up_tmp_file
done
