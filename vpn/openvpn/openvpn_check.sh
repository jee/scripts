#!/bin/bash

#On fait un ping vers www.google.com toutes les 5min, et si aucun paquet n'est reçu en retour (0 packets received, le 0 étant à la position 23 sur la ligne venant de ping) on relance le VPN
#

while [ 1 > 0 ]; do
        sleep 300
	T=`ping -I 192.168.0.1 -c 1 www.google.com |grep "packets transmitted"`
	if [[ ${T:23:1} != "1" ]]
	then 
		/etc/init.d/openvpn restart
	fi
done
