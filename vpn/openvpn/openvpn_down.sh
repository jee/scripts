#! /bin/sh
# This script disables policy routing before the tunnel interface is brought down

#Trouver l'adresse IP affectée à tun0 par le VPN
NET_DEV="tun0"
NET_IP="$(command ifconfig "${NET_DEV}" \
      | command grep 'inet ' \
          | command sed -e 's/^.*inet [^:]*:\([^ ]*\) .*$/\1/')"

#On arrête le script qui va vérifier l'existence du tunnel VPN
killall /root/scripts/vpn/openvpn_check.sh

#On stoppe rtorrent
#/etc/init.d/<user1>-rtorrent stop
#/etc/init.d/<user2>-rtorrent stop
# etc etc...


#On efface les règles iptables ajoutées

iptables -D FORWARD -s 192.168.0.1/32 -o eno1 -j DROP
iptables -D SERVICES -p udp --dport 9339 -j ACCEPT # DHT
iptables -D SERVICES -p tcp --dport 9339:9339> -j ACCEPT # rTorrent random range
iptables -D INPUT -i tun0 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -D POSTROUTING -t nat -o tun0 -j MASQUERADE
iptables -D PREROUTING -t nat -i tun0 -p udp --dport 9339 -j DNAT --to 192.168.0.1
iptables -D PREROUTING -t nat -i tun0 -p tcp --dport 9339:9339 -j DNAT --to 192.168.0.1

# Remove rule for the secondary loopback IP address (192.168.0.1)
ip rule del from 192.168.0.1/32 table VPN

# Remove rule for tun0 IP address
ip rule del from  ${NET_IP}/32 table VPN

# Remove rule for the secondary loopback IP address (192.168.0.1)
ip rule del from 192.168.0.1/32 table VPN

# Remove the default route via tun0 from the IPRED routing table
ip route del default dev tun0 table VPN
