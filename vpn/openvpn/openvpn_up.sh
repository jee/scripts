#!/bin/sh
# This script enables policy routing after the tunnel interface is brought up
# Policy routing is used to make sure response packets go through the tunnel interface
# This is mandatory when your ISP has setup anti-spoofing filters

#Créer une chaine iptables nommée SERVICES, renverra un warning sans conséquences si déjà crée. Pour la propreté de la conf. vous pouvez éventuellement supprimer cette directive et la mettre dans un de vos scripts qui se lance au démarrage du système.

iptables -N SERVICES


#Obtenir l'adresse IP donnée par le VPN à tun0, à supposer que votre vpn crée une interface de ce nom.
NET_DEV="tun0"
NET_IP="$(command ifconfig "${NET_DEV}" \
    | command grep 'inet ' \
    | command sed -e 's/^.*inet [^:]*:\([^ ]*\) .*$/\1/')"



# Add a default route via <tun0> into the VPN routing table
ip route add default dev tun0 table VPN

# Pass traffic from lo:1 (192.168.0.1) to the VPN routing table, using policy routing ("ip rule" commands)
ip rule add from 192.168.0.1/32 table VPN

# Pass traffic from tun0 IP address to the VPN routing table
ip rule add from ${NET_IP}/32 table VPN

#On ajoute les règles iptables pour faire le NAT

 # Source NAT and destination NAT rules
  iptables -A PREROUTING -t nat -i tun0 -p tcp --dport 9339:9339 -j DNAT --to 192.168.0.1
  iptables -A PREROUTING -t nat -i tun0 -p udp --dport 9339 -j DNAT --to 192.168.0.1
  iptables -A POSTROUTING -t nat -o tun0 -j MASQUERADE
  # Allow session continuation traffic
  iptables -A INPUT -i tun0 -m state --state RELATED,ESTABLISHED -j ACCEPT
  # Allow Bittorrent traffic via tun0
  iptables -A SERVICES -p tcp --dport 9339:9339 -j ACCEPT                     # rTorrent random range
  iptables -A SERVICES -p udp --dport 9339 -j ACCEPT                          # DHT
  # Disallow BitTorrent traffic via eth0 - Just to be extra safe [img]/assets/images/smileys/wink.png[/img]
  iptables -A FORWARD -s 192.168.0.1/32 -o eno1 -j DROP

#Démarrage de rtorrent
#/etc/init.d/<user1>-rtorrent start
#/etc/init.d/<user2>-rtorrent start
# etc etc...

#Démarrage du script vérifiant que le VPN est toujours actif
killall /root/scripts/vpn/openvpn_check.sh
/root/scripts/vpn/openvpn_check.sh &
