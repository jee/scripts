#!/bin/bash

echo "Welcome!" 
echo "This scritp will help you to generate a wireguard client config"

read  -p "Please enter the name of the client without spaces or special charset: " clientName

tmp_conf_dir="/tmp/$clientName"


if [ ! -d  $tmp_conf_dir ]; then
  echo "create $tmp_conf_dir"
  mkdir $tmp_conf_dir
else
  echo "ERROR can not create $tmp_conf_dir directory already exist"
  exit 1
fi


cd $tmp_conf_dir

echo "generate keys"
wg genkey > $tmp_conf_dir/private_key
wg pubkey < $tmp_conf_dir/private_key > $tmp_conf_dir/public_key
wg genpsk > $tmp_conf_dir/psk_key   

echo 
