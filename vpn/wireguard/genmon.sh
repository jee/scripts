#!/bin/bash
#set -e

if [ -n "$1" ] || [ -r "$1" ]; then
  source $1
else
  script_full_path=$(readlink -f "$0")
  script_path=`dirname "$script_full_path"`
  conf_path="$script_path/config"
  if [ -f "$conf_path" ] && [ -r "$conf_path" ]; then
    source $conf_path
  else
    echo -e "No conf File found !"
    exit 1
  fi
fi

GREEN_ICON="$script_path/icons/green.png"
YELLOW_ICON="$script_path/icons/yellow.png"
ORANGE_ICON="$script_path/icons/orange.png"
RED_ICON="$script_path/icons/red.png"

check_network () {
  $GET_IP > /dev/null
  NETWORK_CHECK="$?"
}
  
if [ $GET_IP == $VPN_IP ]; then
  echo -e "<click>bash -c 'xfce4-panel --plugin-event=genmon"$GENMON_ID":refresh:bool:true'</click>"
  echo -e "<img>$GREEN_ICON</img>"
  echo -e "<tool><span foreground='green'>$GET_IP</span>\n<i><small>Click on the icon to refresh</small></i></tool>"
elif [ $GET_IP == $REAL_IP ]; then
  echo -e "<click>bash -c 'xfce4-panel --plugin-event=genmon"$GENMON_ID":refresh:bool:true'</click>"
  echo -e "<img>$RED_ICON</img>"
  echo -e "<tool><span foreground='red'>$GET_IP</span>\n<i><small>Click on the icon to refresh</small></i></tool>"
elif [ "$NETWORK_CHECK" != "0" ]; then
  echo -e "<click>xfce4-panel --plugin-event=genmon"$GENMON_ID":refresh:bool:true</click>"
  echo -e "<img>$ORANGE_ICON</img>"
  echo -e "<tool><tt><b><u>Error</u></b> : <span foreground='orange'>Network connectivity issue</span></tt></tool>"
  echo -e "<tool><small>Click on the icon to refresh</small></tool>"
else
  echo -e "<click>xfce4-panel --plugin-event=genmon"$GENMON_ID":refresh:bool:true</click>"
  echo -e "<img>$ORANGE_ICON</img>"
  echo -e "<tool><tt><b><u>Error</u></b> : <span foreground='orange'>There is something wrong!</span></tt></tool>"
  echo -e "<tool><small>Click on the icon to refresh</small></tool>"
fi
exit 0
