#!/bin/sh

Patch_src="($this->set('ROOT_PATH', Util::normalize_path(dirname($this->get('H5AI_PATH')), false));)"
Patch_tgt="($this->set('ROOT_PATH', "/data");)"

sed -i -e "s/${Patch_src}/${Patch_tgt}/g" "$1"

