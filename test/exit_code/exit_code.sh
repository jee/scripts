#!/usr/bin/env bash

set -o pipefail
#
# ls -lsha /home/jee $@ | tee /tmp/exit_code.log
# echo ${PIPESTATUS[0]}
ls -lsha /homezenl/ |& tee --output-error warn -a /tmp/exit_code.log

echo ${PIPESTATUS[0]}
