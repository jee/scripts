#!/bin/bash

#Variables
notify_file="$1"
torrent_id=$(sed -n 1p "$notify_file")
torrent_name=$(sed -n 2p "$notify_file")
#torrent_name=$(printf #%q "$torrent_name")
torrent_path=$(sed -n 3p "$notify_file")
torrent_full_path="$torrent_path/$torrent_name"
torrent_label=$(echo "$torrent_path" | sed 's/\/mnt\/data\/torrents\/seed\///g')
unsorted_path="/mnt/data/Media/Music/.Unsorted/"
unsorted_album_path="$unsorted_path$torrent_name"
flac_path="/mnt/data/Media/Music/FLAC/"
lock_file="/tmp/$torrent_name.lock"
log_file="/logs/auto_beets.log"

# Environments
BEETSDIR=/config
export BEETSDIR
FPCALC=/usr/bin/fpcalc
export FPCALC

#Functions

check_label() {
  case "$torrent_label" in
    "Music")
      echo "$(date +[%F][%T])" "[INFO]" "Beets Import Started     " "[$torrent_label] [$torrent_name]" >> "$log_file"
    ;;
    *)
      echo "$(date +[%F][%T])" "[ERROR]" "Script Aborted Bad Label   " "[$torrent_label] [$torrent_name]" >> "$log_file" 
      exit 0
    ;;
  esac
}

check_lock_file () {
  while [ -f "$lock_file" ]
  do
    echo "$(date +[%F][%T])" "[WARN]" " Lock file Detected    " "[$lock_file] [$torrent_name]"
    sleep 10
  done
}

create_lock_file () {
  touch "$lock_file"
  echo "$(date +[%F][%T])" "[INFO]" " Lock file Created    " "[$lock_file] [$torrent_name]"  >> "$log_file"
}

remove_lock_file () {
  rm -rf "$lock_file"
  echo "$(date +[%F][%T])" "[INFO]" " Lock file Removed    " "[$lock_file] [$torrent_name]"  >> "$log_file"
}

copy_music_to_unsorted () {
 cp -r "$torrent_full_path" "$unsorted_path"
 retval_cp_mu="$?"
  if [ "$retval_cp_mu" == 0 ]; then
    echo "$(date +[%F][%T])" "[INFO]" "Album Succefully copied to Unsorted" "[$torrent_label] [$torrent_name]" >> "$log_file"
  else
    echo "$(date +[%F][%T])" "[ERROR]" "Album Unsuccessfully copied to Unsorted" "[$torrent_label] [$torrent_name]" >> "$log_file"
  fi
}

beets_flac_import () {
  /usr/bin/beet -v -c /config/flac/config.yaml import -q -i "$unsorted_path"
  retval_beets_flac_import="$?"
  if [ "$retval_beets_flac_import" == 0 ]; then
    echo "$(date +[%F][%T])" "[INFO]" "File Successfull Import FLAC" "[$torrent_label] [$torrent_name]" >> "$log_file"
    rm "$notify_file"
  else
    echo "$(date +[%F][%T])" "[ERROR]" "Filebot Failed to Import FLAC" "[$torrent_label] [$torrent_name]" >> "$log_file"
    remove_lock_file
    exit 1
  fi
}

beets_ogg_import () {
  /usr/bin/beet -v -c /config/ogg/config.yaml import -q -i "$flac_path"
  retval_beets_ogg_import="$?"
  if [ "$retval_beets_ogg_import" == 0 ]; then
    echo "$(date +[%F][%T])" "[INFO]" "File Successfull Import    " "[$torrent_label] [$torrent_name]" >> "$log_file"
  else
    echo "$(date +[%F][%T])" "[ERROR]" "Filebot Failed to Import   " "[$torrent_label] [$torrent_name]" >> "$log_file"
  fi
}

beets_opus_import () {
  /usr/bin/beet -v -c /config/opus/config.yaml import -q -i "$flac_path"
  retval_beets_opus_import="$?"
  if [ "$retval_beets_opus_import" == 0 ]; then
    echo "$(date +[%F][%T])" "[INFO]" "File Successfull Import OPUS" "[$torrent_label] [$torrent_name]" >> "$log_file"
  else
    echo "$(date +[%F][%T])" "[ERROR]" "Filebot Failed to Import OPUS" "[$torrent_label] [$torrent_name]" >> "$log_file"
  fi
}

beets_mp3_import () {
  /usr/bin/beet -v -c /config/mp3/config.yaml import -q -i "$flac_path"
  retval_beets_mp3_import="$?"
  if [ "$retval_beets_mp3_import" == 0 ]; then
    echo "$(date +[%F][%T])" "[INFO]" "File Successfull Import    " "[$torrent_label] [$torrent_name]" >> "$log_file"
  else
    echo "$(date +[%F][%T])" "[ERROR]" "Filebot Failed to Import   " "[$torrent_label] [$torrent_name]" >> "$log_file"
  fi
}

clean_unsorted_music () {
  if [ -d "$unsorted_album_path" ]; then
    check_audio_file_in_dir=$(find "$unsorted_album_path" -maxdepth 3 -name "*.flac" -o -name "*.mp3" -o -name "*.ogg" -o -name "*.wav" | wc -l)
    if [ "$check_audio_file_in_dir" != 0 ]; then
      echo "$(date +[%F][%T])" "[WARN]" "Contain Audio Files Process Failed" "[$unsorted_album_path] [$torrent_name]"
    else
      rm -rf "$unsorted_album_path"
      echo "$(date +[%F][%T])" "[INFO]" "Contain No Audio file Dir Removed" "[$unsorted_album_path] [$torrent_name]"
    fi
  fi
}

clean_notify_file () {
  rm "$notify_file"
}

clean_tmp () {
  rm -R /tmp/tmp*
}

main_process () {
  check_label
  check_lock_file
  create_lock_file
  copy_music_to_unsorted
  beets_flac_import
  beets_opus_import
  beets_mp3_import
  clean_unsorted_music
  clean_tmp
  clean_notify_file
  remove_lock_file
  exit 0
}

main_process
