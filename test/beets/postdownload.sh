#!/bin/sh

#Variables
LOCK_FILE="/tmp/postdownload.lock"
POSTDOWNLOAD_LOG_FILE="/home/deluge/logs/postdownload.log"
TORRENT_ID="$1"
TORRENT_NAME="$2"
TORRENT_PATH="$3"
TORRENT_FULLPATH="$TORRENT_PATH/$TORRENT_NAME"
UNSORTED_MUSIC="/mnt/data/Media/Music/Unsorted/"
UNSORTED_ALBUM="$UNSORTED_MUSIC$TORRENT_NAME"
FLAC_PATH="/mnt/data/Media/Music/FLAC/"

#Functions
check_if_already_running () {
  while [ -f "$LOCK_FILE" ]
  do
    sleep 10
  done
}

create_lock_file () {
  touch "$LOCK_FILE"
  echo "File created : ""$(date +[%F][%T])""by" "$TORRENT_NAME" >> "$LOCK_FILE"
  echo "$(date +[%F][%T])"" [""$TORRENT_NAME""] ""$LOCK_FILE" "CREATED" >> "$POSTDOWNLOAD_LOG_FILE"
}

remove_lock_file () {
  rm -rf "$LOCK_FILE"
  echo "$(date +[%F][%T])"" [""$TORRENT_NAME""] ""$LOCK_FILE" "DELETED" >> "$POSTDOWNLOAD_LOG_FILE"
}

copy_music_to_unsorted () {
  if [ "$TORRENT_PATH" = "/mnt/data/Torrents/Seed/Music" ]; then
   cp -r "$TORRENT_FULLPATH" "$UNSORTED_MUSIC"
   echo "$(date +[%F][%T])"" [""$TORRENT_NAME""] " "COPIED TO UNSORTED" >> "$POSTDOWNLOAD_LOG_FILE"
  fi
}

beet_flac_import () {
  beet -vv -c /home/deluge/.config/beets/flac.yaml import "$UNSORTED_ALBUM"
}

beet_ogg_import () {
  beet -vv -c /home/deluge/.config/beets/ogg.yaml import "$FLAC_PATH"
}

beet_opus_import () {
  beet -vv -c /home/deluge/.config/beets/opus.yaml import "$FLAC_PATH"
  }
beet_mp3_import () {
  beet -vv -c /home/deluge/.config/beets/mp3.yaml import "$FLAC_PATH"
}

clean_unsorted_music () {
  if [ -d "$UNSORTED_ALBUM" ]; then
    check_audio_file_in_dir=$(find "$UNSORTED_ALBUM" -maxdepth 3 -name "*.flac" -o -name "*.mp3" -o -name "*.ogg" -o -name "*.wav" | wc -l)
    if [ "$check_audio_file_in_dir" != 0 ]; then
      echo "$UNSORTED_ALBUM" "Contain Audio Files Process Failed" >> "$POSTDOWNLOAD_LOG_FILE"
    else
      rm -rf "$UNSORTED_ALBUM"
      echo "$UNSORTED_ALBUM" "Contain No Audio file Dir Removed" >> "$POSTDOWNLOAD_LOG_FILE"
    fi
  fi
}

clean_tmp () {
  rm -R /tmp/tmp*
}

main_postprocess () {
  case "$TORRENT_PATH" in
    "/mnt/data/Torrents/Seed/Music")
      check_if_already_running
      create_lock_file
      copy_music_to_unsorted
      beet_flac_import
      beet_opus_import
      beet_mp3_import
      clean_tmp
      remove_lock_file
      clean_unsorted_music
    ;;
    "*")
      echo "[SKIPED] " "$TORRENT_NAME" "Is not in Music Directory" >> "$POSTDOWNLOAD_LOG_FILE"
    ;;
  esac
exit 0
}

main_postprocess