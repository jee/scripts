#!/bin/sh

for album_dir in /mnt/data/Media/Music/.Unsorted/*
do
    check_audio_file_in_dir=$(find "$album_dir" -maxdepth 3 -name "*.flac" -o -name "*.mp3" -o -name "*.ogg" -o -name "*.wav" | wc -l)
    if [ "$check_audio_file_in_dir" = 0 ]; then
        rm -rf "$album_dir"
        echo  "$album_dir contain no audio files"
    fi
done

