#!/bin/sh

func_watch () {
	inotifywait -m \
	-e create,moved_to \
	--format '%w%f' \
        /tmp/watch_dir/
}

func_watch | \
while IFS= read -r file; do
    echo "$file" 
done
