#!/bin/sh

#Function

info_artist () {
  mediainfo --Inform="General;%Performer%" "$FILE"
}

info_album () {
  mediainfo --Inform="General;%Album%"
}

info_year () {
  mediainfo --Inform="General;%Recorded_Date%"
}

info_label () {
  mediainfo --Inform="General;%Label%"
}

info_format () {
  mediainfo --Inform="Audio;%Format%"
}

info_bitrate_mode () {
  mediainfo --Inform="General;%BitRate_Mode%"
}

info_track_number () {
  mediainfo --Inform="General;%Track/Position%"
}

info_track_title () {
  mediainfo --Inform="General;%Track%"
}

info_track_lenght () {
  mediainfo --Inform="Audio;%Duration/String3%"
}

info_track_bitrate () {
  mediainfo --Inform="General;%BitRate/String%"
}

#nfo_artist#album_info () {
#  for filename in "$dir"
#  do
#    case "filename" in
#      [ $(info_track_number) = "1" ] )
#        echo "Artist     :" "info_artis" "$filename"
#        echo "Album      :" "info_album" "$filename"
#        echo "Year       :" "info_year" "$filename"
#        echo "Label      :" "info_label" "$filename"
#        echo "Codec      :" "info_format" "$filename"
#        ;;
#    esac
#  done
#}
FILE="$1"
echo "Artist     :" "$(info_artist)" "$FILE"
echo "Album      :" info_album $filename
echo "Year       :" "info_year" "$filename"
echo "Label      :" "info_label" "$filename"
echo "Codec      :" "info_format" "$filename"
