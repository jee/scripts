#!/bin/bash
hexo_pid="$(pidof hexo)"

inotifywait -r -m -e modify,create,close_write --format '%w%f' /home/jee/Stuff/Git/jeer.fr | while IFS= read -r dir_path; do
        if [[ ! -z "$hexo_pid" ]]; then 
	  killall hexo 
	  hexo clean
	  hexo server &
	else 
	  hexo clean
	  hexo server &
	fi
done

