#!/bin/bash
sh /home/osmc/.filebot/filebot.sh -script fn:amc --output "/home/osmc/media" --log-file amc.log --action keeplink --conflict override -non-strict --def "seriesFormat=TV\ Shows/{n}/{episode.special ? 'Special' : 'Season '+s.pad(2)}/{fn}" "movieFormat=Movies/{n} ({y})/{fn}" music=y artwork=y "ut_dir=$TR_TORRENT_DIR/$TR_TORRENT_NAME" "ut_kind=multi" "ut_title=$TR_TORRENT_NAME"
