#!/bin/bash

source ./var_example

for v in v0p1p1 v3p2p0p1 v1p3 ; do
    v=${v//[vp]/.}             # Replace v's and p's with dots.
    v=${v#.}                   # Remove the leading dot.
    echo "$v"
done
