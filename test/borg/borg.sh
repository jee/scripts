#!/bin/bash
#
# Script de sauvegarde.
#
# Envoie les sauvegardes sur un serveur distant, via le programme borg.
# Les sauvegardes sont chiffrées
#
# http://borgbackup.readthedocs.org/
#
# Sauvegarde :
# - mails
# - config
# - /home
#
# Est lancé quotidiennement.

set -e

if [ -n "$1" ] || [ -r "$1" ]; then
  source $1
else
  script_full_path=$(readlink -f "$0")
  script_path=`dirname "$script_full_path"`
  conf_path="$script_path/borg.conf"
  if [ -n "$conf_path" ] || [ -r "$conf_path" ]; then
    source $conf_path
  else
    echo "No conf File found !"
    echo "No conf File found !" | mail -s "Backup $BACKUP_DATE" $EMAIL_ADDRESS
    exit1
  fi
fi

export BORG_PASSPHRASE="$BORG_PASSPHRASE"

cleanup()
{
    echo "Something bad happened during backup, check ${TMP_LOG_FILE}"
    cat "$TMP_LOG_FILE" | mail -s "Backup $BACKUP_DATE" $EMAIL_ADDRESS
    cat $TMP_LOG_FILE >> $LOG_FILE
    rm $TMP_LOG_FILE
    rm $TMP_DPKG_LIST_FILE
    rm $TMP_MYSQL_DUMP_FILE
    umount $FTP_MOUNT_POINT
    exit $1
}

ts_log()
{
    echo `date '+%Y-%m-%d %H:%m:%S'` $1 >> ${TMP_LOG_FILE}
}

mount_ftp () {

  if [[ $(findmnt -M "$FTP_MOUNT_POINT") ]]; then
    umount $FTP_MOUNT_POINT
    curlftpfs -v $FTP_HOST $FTP_MOUNT_POINT -o user=$FTP_USER:$FTP_PASSWORD -o allow_root
  else 
    curlftpfs -v $FTP_HOST $FTP_MOUNT_POINT -o user=$FTP_USER:$FTP_PASSWORD -o allow_root
  fi
}

unmout_ftp () {
  umount $FTP_MOUNT_POINT
}

sync_local_to_remote () {
  rsync --archive --itemize-changes --temp-dir=/mnt/data/backup/tmp --exclude=keepall --delete-before --no-perms --checksum $BORG_REPOSITORY/ $FTP_MOUNT_POINT >> ${TMP_LOG_FILE} 2>&1
}

# Trap on non-zero exit
trap '[ "$?" -eq 0 ] || cleanup' EXIT

ts_log "Starting new backup ${BACKUP_DATE}..."

ts_log 'Dumping MySQL db...'
mysqldump --all-databases --events -p$MYSQL_ROOT_PASS > $TMP_MYSQL_DUMP_FILE

ts_log 'Dumping PostgreSQL db...'
pg_dumpall > $TMP_PSQL_DUMP_FILE

ts_log 'dpkg list intalled packages'
/usr/bin/dpkg --get-selections > $TMP_DPKG_LIST_FILE

#ts_log 'Dumping LDAP...'
#slapcat -l $TMP_LDAP_DUMP_FILE

ts_log "Pushing archive ${BORG_ARCHIVE}"
$BORG create \
     -v --stats --compression lzma,9 \
     --one-file-system \
     --exclude-caches \
     --exclude '/home/*/.cache/*' \
     --exclude '/home/*/Media/' \
     --exclude '/home/*/torrents/' \
     --exclude '/var/cache/*' \
     --exclude '/var/tmp/*' \
     $BORG_ARCHIVE \
     /etc \
     /var \
     /home \
     /var \
     /root \
     /opt \
     $TMP_MYSQL_DUMP_FILE \
     $TMP_PSQL_DUMP_FILE \
     $TMP_DPKG_LIST_FILE \
     >> ${TMP_LOG_FILE} 2>&1

ts_log "Rotating old backups."
$BORG prune -v $BORG_REPOSITORY \
      --keep-daily=7 \
      --keep-weekly=4 \
      --keep-monthly=6 \
      >> ${TMP_LOG_FILE} 2>&1

ts_log "Mount FTP..."
mount_ftp
ts_log "Sync local to remote..."
sync_local_to_remote
ts_log "Unmount FTP..."
unmout_ftp
ts_log 'Send mail report...'
ts_log 'Cleaning up temp files...'
cat "$TMP_LOG_FILE" | mail -s "Backup $BACKUP_DATE" "$EMAIL_ADDRESS"
cat $TMP_LOG_FILE >> $LOG_FILE
rm $TMP_LOG_FILE 
rm $TMP_DPKG_LIST_FILE
rm $TMP_MYSQL_DUMP_FILE
#rm $TMP_LDAP_DUMP_FILE
